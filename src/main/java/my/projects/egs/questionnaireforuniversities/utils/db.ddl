create schema if not exists questionnaire_for_universities_db collate utf8mb4_0900_ai_ci;

create table if not exists degree
(
    id smallint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    degree_name varchar(255) null,
    constraint UK_bhbcxx2jqgfuwmsx2jtx6kf8o
        unique (degree_name)
);

create table if not exists question_category
(
    id int auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    category_name varchar(255) null,
    constraint UK_snygd3i3gl7o2gk0qnptxpwg1
        unique (category_name)
);

create table if not exists role
(
    id tinyint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    role_name varchar(255) null,
    constraint UK_iubw515ff0ugtm28p8g3myt0h
        unique (role_name)
);

create table if not exists university
(
    id int auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    university_name varchar(255) null,
    constraint UK_f4eogdcmmwr1jl80i3e71d2vv
        unique (university_name)
);

create table if not exists faculty
(
    id int auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    faculty_name varchar(255) null,
    university_id int null,
    constraint FKivqbiytd9en6sk09duabc6scc
        foreign key (university_id) references university (id)
);

create table if not exists user
(
    type varchar(31) not null,
    id bigint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    birth_date date null,
    city varchar(255) null,
    country varchar(255) null,
    email varchar(255) null,
    first_name varchar(255) null,
    last_name varchar(255) null,
    login varchar(255) null,
    password_hash varchar(255) null,
    password_salt varchar(255) null,
    phone varchar(255) null,
    region varchar(255) null,
    street varchar(255) null,
    course tinyint null,
    degree_id smallint null,
    faculty_id int null,
    role_id tinyint null,
    university_id int null,
    constraint UK_589idila9li6a4arw1t8ht1gx
        unique (phone),
    constraint UK_ob8kqyqqgmefl0aco34akdtpe
        unique (email),
    constraint user_login_uindex
        unique (login),
    constraint FK25mtxsyu84lnn33ygespegy2l
        foreign key (faculty_id) references faculty (id),
    constraint FKfgcbu6cj5oqlmnrocb7ctc7wl
        foreign key (degree_id) references degree (id),
    constraint FKic5iffsyggke974jb0jbdb0m
        foreign key (university_id) references university (id),
    constraint FKn82ha3ccdebhokx3a8fgdqeyy
        foreign key (role_id) references role (id)
);

create table if not exists question
(
    id bigint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    question_text varchar(255) null,
    question_title varchar(255) null,
    question_category_id int null,
    user_id bigint null,
    constraint FK4ekrlbqiybwk8abhgclfjwnmc
        foreign key (user_id) references user (id),
    constraint FKof6avkj3vu921wcnx85gjnw7c
        foreign key (question_category_id) references question_category (id)
);

create table if not exists answer
(
    id bigint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    answer_text varchar(255) null,
    question_id bigint null,
    user_id bigint null,
    constraint FK68tbcw6bunvfjaoscaj851xpb
        foreign key (user_id) references user (id),
    constraint FK8frr4bcabmmeyyu60qt7iiblo
        foreign key (question_id) references question (id)
);

create table if not exists answer_comment
(
    id bigint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    comment_text varchar(255) null,
    answer_id bigint null,
    user_id bigint null,
    constraint FK87c9uh8328afrd7v20x4hcfar
        foreign key (user_id) references user (id),
    constraint FKbi4ojf23mofyyc9wmmslub4ln
        foreign key (answer_id) references answer (id)
);

create table if not exists question_comment
(
    id bigint auto_increment
        primary key,
    created_at date null,
    updated_at date null,
    comment_text varchar(255) null,
    question_id bigint null,
    user_id bigint null,
    constraint FK5v1x6rdm2bocicjn0c5ndow8c
        foreign key (user_id) references user (id),
    constraint FKgatho66t7ix04m6dothg6jaqh
        foreign key (question_id) references question (id)
);