package my.projects.egs.questionnaireforuniversities.utils;

public enum UserType {
    STUDENT,
    LECTURER
}
