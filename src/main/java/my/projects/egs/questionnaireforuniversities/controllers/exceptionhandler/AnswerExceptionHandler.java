package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswerNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswersNotFoundForQuestionException;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswersNotFoundForUserException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class AnswerExceptionHandler {

    @ExceptionHandler(AnswerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error answerNotFound(AnswerNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Answer with " + exception.getAnswerId() + " id not found");
    }

    @ExceptionHandler(AnswersNotFoundForQuestionException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error answersByCategoryNotFound(AnswersNotFoundForQuestionException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Answers in " + exception.getQuestionId() + " question id not found");
    }

    @ExceptionHandler(AnswersNotFoundForUserException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionsByUserNotFound(AnswersNotFoundForUserException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "User with " + exception.getUserId() + " id has not answers");
    }
}
