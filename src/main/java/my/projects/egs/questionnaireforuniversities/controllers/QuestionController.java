package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundForCategoryException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundForUserException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundException;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionController {

    private QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @RequestMapping(value = "/questions", method = RequestMethod.POST)
    public QuestionDTO saveQuestion(@Valid @RequestBody QuestionDTO questionDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return questionService.save(questionDTO);
    }

    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public List<QuestionDTO> getAllQuestions() {
        List<QuestionDTO> questions = questionService.findAll();
        if (!questions.isEmpty()) {
            return questions;
        }
        throw new QuestionsNotFoundException();
    }

    @RequestMapping(value = "/categories/{category}/questions", method = RequestMethod.GET)
    public List<QuestionDTO> getQuestionsByCategory(@PathVariable(name = "category") String categoryName) {
        List<QuestionDTO> categoryQuestions = questionService.findByCategoryName(categoryName);
        if (!categoryQuestions.isEmpty()) {
            return categoryQuestions;
        }
        throw new QuestionsNotFoundForCategoryException(categoryName);
    }

    @RequestMapping(value = "/users/{id}/questions", method = RequestMethod.GET)
    public List<QuestionDTO> getQuestionsByUserLogin(@PathVariable long id) {
        List<QuestionDTO> userQuestions = questionService.findByUserId(id);
        if (!userQuestions.isEmpty()) {
            return userQuestions;
        }
        throw new QuestionsNotFoundForUserException(id);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.PATCH)
    public QuestionDTO updateQuestion(@PathVariable long id, @RequestBody QuestionDTO questionDTO) {
        if (questionService.findByID(id) != null) {
            questionDTO.setId(id);
            return questionService.save(questionDTO);
        }
        throw new QuestionNotFoundException(id);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.GET)
    public QuestionDTO getQuestion(@PathVariable long id) {
        return questionService.findByID(id);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.DELETE)
    public void deleteQuestion(@PathVariable long id) {
        if (questionService.findByID(id) != null) {
            questionService.deleteById(id);
        }else throw new QuestionNotFoundException(id);
    }
}
