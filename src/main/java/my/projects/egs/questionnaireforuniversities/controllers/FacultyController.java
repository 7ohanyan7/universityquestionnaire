package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultiesNotFoundForUniversityException;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultiesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultyNotFoundException;
import my.projects.egs.questionnaireforuniversities.sevices.FacultyService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class FacultyController {

    private FacultyService facultyService;

    public FacultyController(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @RequestMapping(value = "/faculties", method = RequestMethod.POST)
    public FacultyDTO saveFaculty(@Valid @RequestBody FacultyDTO facultyDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return facultyService.save(facultyDTO);
    }

    @RequestMapping(value = "/faculties", method = RequestMethod.GET)
    public List<FacultyDTO> getAllFaculties() {
        List<FacultyDTO> faculties = facultyService.findAll();
        if (!faculties.isEmpty()) {
            return faculties;
        }
        throw new FacultiesNotFoundException();
    }

    @RequestMapping(value = "/universities/{university}/faculties", method = RequestMethod.GET)
    public List<FacultyDTO> getFacultiesByUniversity(@PathVariable(name = "university") String universityName) {
        List<FacultyDTO> faculties = facultyService.findByUniversityName(universityName);
        if (!faculties.isEmpty()) {
            return faculties;
        }
        throw new FacultiesNotFoundForUniversityException(universityName);
    }

    @RequestMapping(value = "/faculties/{id}", method = RequestMethod.PATCH)
    public FacultyDTO updateFaculty(@PathVariable int id, @RequestBody FacultyDTO facultyDTO) {
        if (facultyService.findByID(id) != null) {
            facultyDTO.setId(id);
            return facultyService.save(facultyDTO);
        }
        throw new FacultyNotFoundException(id);
    }

    @RequestMapping(value = "/faculties/{id}", method = RequestMethod.GET)
    public FacultyDTO getFaculty(@PathVariable int id) {
        FacultyDTO faculty = facultyService.findByID(id);
        if (faculty != null) {
            return faculty;
        }
        throw new FacultyNotFoundException(id);
    }

    @RequestMapping(value = "/faculties/{id}", method = RequestMethod.DELETE)
    public void deleteQuestion(@PathVariable int id) {
        if (facultyService.findByID(id) != null) {
            facultyService.deleteById(id);
        }else throw new FacultyNotFoundException(id);
    }

}
