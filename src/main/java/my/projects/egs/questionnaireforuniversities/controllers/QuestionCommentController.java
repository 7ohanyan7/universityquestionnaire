package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCommentDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.QuestionCommentNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.QuestionCommentsNotFoundForQuestionException;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionCommentService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionCommentController {

    private QuestionCommentService questionCommentService;

    public QuestionCommentController(QuestionCommentService questionCommentService) {
        this.questionCommentService = questionCommentService;
    }

    @PostMapping(value = "/questioncomments")
    public QuestionCommentDTO saveComment(@Valid @RequestBody QuestionCommentDTO questionCommentDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return questionCommentService.save(questionCommentDTO);
    }

    @GetMapping(value = "/questioncomments/{id}")
    public QuestionCommentDTO getComment(@PathVariable long id) {
        QuestionCommentDTO comment = questionCommentService.findByID(id);
        if (comment != null) {
            return comment;
        }
        throw new QuestionCommentNotFoundException(id);
    }

    @PutMapping(value = "/questioncomments/{id}")
    public QuestionCommentDTO updateComment(@PathVariable long id, @RequestBody QuestionCommentDTO questionCommentDTO) {
        if (questionCommentService.findByID(id) != null) {
            questionCommentDTO.setId(id);
            return questionCommentService.save(questionCommentDTO);
        }
        throw new QuestionCommentNotFoundException(id);
    }

    @DeleteMapping(value = "/questioncomments/{id}")
    public void deleteComment(@PathVariable long id) {
        if (questionCommentService.findByID(id) != null) {
            questionCommentService.deleteById(id);
        }else throw new QuestionCommentNotFoundException(id);
    }

    @GetMapping(value = "/questions/{id}/comments")
    public List<QuestionCommentDTO> getCommentsByQuestion(@PathVariable long id) {
        List<QuestionCommentDTO> comments = questionCommentService.findByQuestionId(id);
        if (!comments.isEmpty()) {
            return comments;
        }
        throw new QuestionCommentsNotFoundForQuestionException(id);
    }

}
