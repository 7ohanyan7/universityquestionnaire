package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.universityreleated.UniversitiesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.universityreleated.UniversityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class UniversityExceptionHandler {

    @ExceptionHandler(UniversityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error universityNotFound(UniversityNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "University with " + exception.getUniversityId() + " id not found");
    }

    @ExceptionHandler(UniversitiesNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error universitiesNotFound(UniversitiesNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Universities not found");
    }

}
