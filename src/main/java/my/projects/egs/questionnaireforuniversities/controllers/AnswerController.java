package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswerNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswersNotFoundForQuestionException;
import my.projects.egs.questionnaireforuniversities.exceptions.answerreleated.AnswersNotFoundForUserException;
import my.projects.egs.questionnaireforuniversities.sevices.AnswerService;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AnswerController {

    private AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    @PostMapping(value = "/answers", produces = MediaType.APPLICATION_JSON_VALUE)
    public AnswerDTO saveAnswer(@Valid @RequestBody AnswerDTO answerDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return answerService.save(answerDTO);
    }

    @GetMapping(value = "/users/{id}/answers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AnswerDTO> getAnswersByUser(@PathVariable(name = "id") long id) {
        List<AnswerDTO> answers = answerService.findByUserId(id);
        if (!answers.isEmpty()) {
            return answers;
        }
        throw new AnswersNotFoundForUserException(id);
    }

    @PutMapping(value = "/answers/{id}")
    public AnswerDTO updateAnswer(@PathVariable int id, @RequestBody AnswerDTO answerDTO) {
        if (answerService.findByID(id) != null) {
            answerDTO.setId(id);
            return answerService.save(answerDTO);
        }
        throw new AnswerNotFoundException(id);
    }

    @GetMapping(value = "/questions/{id}/answers")
    public List<AnswerDTO> getAnswersByQuestion(@PathVariable long id) {
        List<AnswerDTO> answers = answerService.findByQuestionID(id);
        if (!answers.isEmpty()) {
            return answers;
        }
        throw new AnswersNotFoundForQuestionException(id);
    }

    @DeleteMapping(value = "/answers/{id}")
    public void deleteAnswer(@PathVariable int id) {
        if (answerService.findByID(id) != null) {
            answerService.deleteById(id);
        }else throw new AnswerNotFoundException(id);
    }
}
