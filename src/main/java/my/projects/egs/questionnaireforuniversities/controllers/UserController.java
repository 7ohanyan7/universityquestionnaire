package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.LecturerResponseDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.StudentResponseDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.userreleated.*;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.sevices.LecturerService;
import my.projects.egs.questionnaireforuniversities.sevices.StudentService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {

    private StudentService studentService;
    private LecturerService lecturerService;
    private UserRepository userRepository;

    public UserController(StudentService studentService, LecturerService lecturerService, UserRepository userRepository) {
        this.studentService = studentService;
        this.lecturerService = lecturerService;
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public StudentResponseDTO saveStudent(@Valid @RequestBody StudentRequestDTO studentRequestDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return studentService.save(studentRequestDTO);
    }

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public List<StudentResponseDTO> getStudents() {
        System.out.println(userRepository.findById(1l));
        System.out.println(userRepository.findById(4l));
        System.out.println(userRepository.findById(5l));
        List<StudentResponseDTO> students = studentService.findAll();
        if (!students.isEmpty()) {
            return students;
        }
        throw new StudentsNotFoundException();
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.PATCH)
    public StudentResponseDTO updateStudent(@PathVariable long id, @RequestBody StudentRequestDTO studentRequestDTO) {
        if (studentService.findByID(id) != null) {
            studentRequestDTO.setId(id);
            return studentService.save(studentRequestDTO);
        }
        throw new StudentNotFoundException(id);
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.GET)
    public StudentResponseDTO getStudent(@PathVariable long id) {
        if (studentService.findByID(id) != null) {
            return studentService.findByID(id);
        }
        throw new StudentNotFoundException(id);
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
    public void deleteStudent(@PathVariable long id) {
        if (studentService.findByID(id) != null) {
            studentService.deleteById(id);
        }else throw new StudentNotFoundException(id);
    }

    @RequestMapping(value = "/faculties/{faculty}/students", method = RequestMethod.GET)
    public List<StudentResponseDTO> getStudentsByFacultyName(@PathVariable(name = "faculty") String facultyName) {
        List<StudentResponseDTO> facultyStudents = studentService.findByFacultyName(facultyName);
        if (!facultyStudents.isEmpty()) {
            return facultyStudents;
        }
        throw new StudentsNotFoundInFacultyException(facultyName);
    }

    @RequestMapping(value = "/universities/{university}/students", method = RequestMethod.GET)
    public List<StudentResponseDTO> getStudentsByUniversityName(@PathVariable(name = "university") String universityName) {
        List<StudentResponseDTO> universityStudents = studentService.findByUniversityName(universityName);
        if (!universityStudents.isEmpty()) {
            return universityStudents;
        }
        throw new StudentsNotFoundInUniversityException(universityName);
    }

    @RequestMapping(value = "/roles/{role}/students", method = RequestMethod.GET)
    public List<StudentResponseDTO> getStudentsByRoleName(@PathVariable(name = "role") String roleName) {
        List<StudentResponseDTO> roleStudents = studentService.findByRoleName(roleName);
        if (!roleStudents.isEmpty()) {
            return roleStudents;
        }
        throw new StudentsNotFoundByRoleException(roleName);
    }

    @RequestMapping(value = "/degrees/{degree}/students", method = RequestMethod.GET)
    public List<StudentResponseDTO> getStudentsByDegreeName(@PathVariable(name = "degree") String degreeName) {
        List<StudentResponseDTO> degreeStudents = studentService.findByDegreeName(degreeName);
        if (!degreeStudents.isEmpty()) {
            return degreeStudents;
        }
        throw new StudentsNotFoundByDegreeException(degreeName);
    }

    @RequestMapping(value = "/lecturers", method = RequestMethod.POST, consumes = "application/json")
    public LecturerResponseDTO saveLecturer(@Valid @RequestBody LecturerRequestDTO lecturerRequestDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return lecturerService.save(lecturerRequestDTO);
    }

    @RequestMapping(value = "/lecturers", method = RequestMethod.GET)
    public List<LecturerResponseDTO> getLecturers() {
        List<LecturerResponseDTO> lecturers = lecturerService.findAll();
        if (!lecturers.isEmpty()) {
            return lecturers;
        }
        throw new LecturersNotFoundException();
    }

    @RequestMapping(value = "/lecturers/{id}", method = RequestMethod.PATCH)
    public LecturerResponseDTO updateLecturer(@PathVariable long id, @RequestBody LecturerRequestDTO lecturerRequestDTO) {
        if (lecturerService.findByID(id) != null) {
            lecturerRequestDTO.setId(id);
            return lecturerService.save(lecturerRequestDTO);
        }
        throw new LecturerNotFoundException(id);
    }

    @RequestMapping(value = "/lecturer/{id}", method = RequestMethod.GET)
    public LecturerResponseDTO getLecturer(@PathVariable long id) {
        LecturerResponseDTO lecturer = lecturerService.findByID(id);
        if (lecturer != null) {
            return lecturer;
        }
        throw new LecturerNotFoundException(id);
    }

    @RequestMapping(value = "/lecturers/{id}", method = RequestMethod.DELETE)
    public void deleteLecturer(@PathVariable long id) {
        if (lecturerService.findByID(id) != null) {
            lecturerService.deleteById(id);
        }else throw new LecturerNotFoundException(id);
    }

    @RequestMapping(value = "/faculties/{faculty}/lecturers", method = RequestMethod.GET)
    public List<LecturerResponseDTO> getLecturersByFacultyName(@PathVariable(name = "faculty") String facultyName) {
        List<LecturerResponseDTO> facultyLecturers = lecturerService.findByFacultyName(facultyName);
        if (!facultyLecturers.isEmpty()) {
            return facultyLecturers;
        }
        throw new LecturersNotFoundInFacultyException(facultyName);
    }

    @RequestMapping(value = "/universities/{university}/lecturers", method = RequestMethod.GET)
    public List<LecturerResponseDTO> getLecturersByUniversityName(@PathVariable(name = "university") String universityName) {
        List<LecturerResponseDTO> universityLecturers = lecturerService.findByUniversityName(universityName);
        if (!universityLecturers.isEmpty()) {
            return universityLecturers;
        }
        throw new LecturersNotFoundInUniversityException(universityName);
    }

    @RequestMapping(value = "/roles/{role}/lecturers", method = RequestMethod.GET)
    public List<LecturerResponseDTO> getLecturersByRoleName(@PathVariable(name = "role") String roleName) {
        List<LecturerResponseDTO> roleLecturers = lecturerService.findByRoleName(roleName);
        if (!roleLecturers.isEmpty()) {
            return roleLecturers;
        }
        throw new LecturersNotFoundByRoleException(roleName);
    }

    @RequestMapping(value = "/degrees/{degree}/lecturers", method = RequestMethod.GET)
    public List<LecturerResponseDTO> getLecturersByDegreeName(@PathVariable(name = "degree") String degreeName) {
        List<LecturerResponseDTO> degreeLecturers = lecturerService.findByDegreeName(degreeName);
        if (!degreeLecturers.isEmpty()) {
            return degreeLecturers;
        }
        throw new LecturersNotFoundByDegreeException(degreeName);
    }
}
