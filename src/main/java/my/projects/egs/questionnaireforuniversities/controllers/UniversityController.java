package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.universityreleated.UniversitiesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.universityreleated.UniversityNotFoundException;
import my.projects.egs.questionnaireforuniversities.sevices.UniversityService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UniversityController {

    private UniversityService universityService;

    public UniversityController(UniversityService universityService) {
        this.universityService = universityService;
    }

    @RequestMapping(value = "/universities", method = RequestMethod.POST)
    public UniversityDTO saveUniversity(@Valid @RequestBody UniversityDTO universityDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return universityService.save(universityDTO);
    }

    @RequestMapping(value = "/universities", method = RequestMethod.GET)
    public List<UniversityDTO> getAllUniversities() {
        List<UniversityDTO> universities = universityService.findAll();
        if (!universities.isEmpty()) {
            return universities;
        }
        throw new UniversitiesNotFoundException();
    }

    @RequestMapping(value = "/universities/{id}", method = RequestMethod.PATCH)
    public UniversityDTO updateUniversity(@PathVariable int id, @RequestBody UniversityDTO universityDTO) {
        if (universityService.findByID(id) != null) {
            universityDTO.setId(id);
            return universityService.save(universityDTO);
        }
        throw new UniversityNotFoundException(id);
    }

    @RequestMapping(value = "/universities/{id}", method = RequestMethod.GET)
    public UniversityDTO getUniversity(@PathVariable int id) {
        UniversityDTO university = universityService.findByID(id);
        if (university != null) {
            return university;
        }
        throw new UniversityNotFoundException(id);
    }

    @RequestMapping(value = "/universities/{id}", method = RequestMethod.DELETE)
    public void deleteUniversity(@PathVariable int id) {
        if (universityService.findByID(id) != null) {
            universityService.deleteById(id);
        }else throw new UniversityNotFoundException(id);
    }
}
