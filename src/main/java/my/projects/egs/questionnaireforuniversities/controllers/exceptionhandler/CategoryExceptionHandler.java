package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.categoryreleated.CategoriesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.categoryreleated.CategoryNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class CategoryExceptionHandler {

    @ExceptionHandler(CategoryNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error categoryNotFound(CategoryNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Category with " + exception.getCategoryId() + " id not found");
    }

    @ExceptionHandler(CategoriesNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error categoriesNotFound(CategoriesNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Categories not found");
    }
}
