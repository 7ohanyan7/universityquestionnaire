package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RestController
public class GeneralExceptionHandler {

    @ExceptionHandler(SaveException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public List<Error> saveExceptionHandler(SaveException exception) {
        List<Error> errors = new ArrayList<>();
        for (ObjectError error : exception.getErrors().getAllErrors()) {
            errors.add(new Error(HttpStatus.NOT_ACCEPTABLE.value(), error.getDefaultMessage()));
        }
        return errors;
    }
}
