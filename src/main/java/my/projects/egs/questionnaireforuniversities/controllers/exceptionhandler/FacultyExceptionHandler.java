package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultiesNotFoundForUniversityException;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultiesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated.FacultyNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class FacultyExceptionHandler {

    @ExceptionHandler(FacultyNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error facultyNotFound(FacultyNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Faculty with " + exception.getFacultyId() + " id not found");
    }

    @ExceptionHandler(FacultiesNotFoundForUniversityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error facultiesByUniversityNotFound(FacultiesNotFoundForUniversityException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Faculties in " + exception.getUniversityName() + " not found");
    }

    @ExceptionHandler(FacultiesNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error facultiesNotFound(FacultiesNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Faculties not found");
    }

}
