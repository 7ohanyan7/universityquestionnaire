package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundForCategoryException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundForUserException;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionsNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class QuestionExceptionHandler {

    @ExceptionHandler(QuestionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionNotFound(QuestionNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Question with " + exception.getQuestionId() + " id not found");
    }

    @ExceptionHandler(QuestionsNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionsNotFound(QuestionsNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Questions not found");
    }

    @ExceptionHandler(QuestionsNotFoundForCategoryException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionsByCategoryNotFound(QuestionsNotFoundForCategoryException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Question with " + exception.getCategoryName() + " category not found");
    }

    @ExceptionHandler(QuestionsNotFoundForUserException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionsByUserNotFound(QuestionsNotFoundForUserException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "User with " + exception.getUserId() + " id has not questions");
    }

}
