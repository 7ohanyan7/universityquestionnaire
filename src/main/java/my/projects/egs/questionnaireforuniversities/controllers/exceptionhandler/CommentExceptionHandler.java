package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.AnswerCommentNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.AnswerCommentsNotFoundForAnswerException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.QuestionCommentNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.QuestionCommentsNotFoundForQuestionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class CommentExceptionHandler {

    @ExceptionHandler(QuestionCommentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionCommentNotFound(QuestionCommentNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Question comment with " + exception.getQuestionCommentId() + " id not found");
    }

    @ExceptionHandler(QuestionCommentsNotFoundForQuestionException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error questionCommentByQuestionNotFound(QuestionCommentsNotFoundForQuestionException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Comments for " + exception.getQuestionId() + " id question not found");
    }

    @ExceptionHandler(AnswerCommentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error answerCommentNotFound(AnswerCommentNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Answer comment with " + exception.getAnswerCommentId() + " id not found");
    }

    @ExceptionHandler(AnswerCommentsNotFoundForAnswerException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error answerCommentByQuestionNotFound(AnswerCommentsNotFoundForAnswerException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Comments for " + exception.getAnswerCommentId() + " id answer not found");
    }
}
