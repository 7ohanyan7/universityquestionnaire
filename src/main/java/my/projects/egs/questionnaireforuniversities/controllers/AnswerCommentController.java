package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerCommentDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.AnswerCommentNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.commentreleated.AnswerCommentsNotFoundForAnswerException;
import my.projects.egs.questionnaireforuniversities.sevices.AnswerCommentService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

public class AnswerCommentController {

    private AnswerCommentService answerCommentService;

    public AnswerCommentController(AnswerCommentService answerCommentService) {
        this.answerCommentService = answerCommentService;
    }

    @PostMapping(value = "/answercomments")
    public AnswerCommentDTO saveComment(@Valid @RequestBody AnswerCommentDTO answerCommentDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return answerCommentService.save(answerCommentDTO);
    }

    @GetMapping(value = "/answercomments/{id}")
    public AnswerCommentDTO getComment(@PathVariable long id) {
        return answerCommentService.findByID(id);
    }

    @PutMapping(value = "/answercomments/{id}")
    public AnswerCommentDTO updateComment(@PathVariable long id, @RequestBody AnswerCommentDTO answerCommentDTO) {
        if (answerCommentService.findByID(id) != null) {
            answerCommentDTO.setId(id);
            return answerCommentService.save(answerCommentDTO);
        }
        throw new AnswerCommentNotFoundException(id);
    }

    @DeleteMapping(value = "/answercomments/{id}")
    public void deleteComment(@PathVariable long id) {
        if (answerCommentService.findByID(id) != null) {
            answerCommentService.deleteById(id);
        }else throw new AnswerCommentNotFoundException(id);
    }

    @GetMapping(value = "/answers/{id}/comments")
    public List<AnswerCommentDTO> getCommentsByAnswer(@PathVariable long id) {
        List<AnswerCommentDTO> comments = answerCommentService.findByAnswerId(id);
        if (!comments.isEmpty()) {
            return comments;
        }
        throw new AnswerCommentsNotFoundForAnswerException(id);
    }
}
