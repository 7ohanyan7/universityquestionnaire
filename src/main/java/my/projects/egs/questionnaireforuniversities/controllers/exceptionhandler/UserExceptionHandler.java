package my.projects.egs.questionnaireforuniversities.controllers.exceptionhandler;

import my.projects.egs.questionnaireforuniversities.error.Error;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.userreleated.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@RestController
public class UserExceptionHandler {

    @ExceptionHandler(LecturersNotFoundInFacultyException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturersNotFoundInFaculty(LecturersNotFoundInFacultyException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturers in " + exception.getFacultyName() + " faculty not found");
    }

    @ExceptionHandler(LecturersNotFoundInUniversityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturersNotFoundInUniversity(LecturersNotFoundInUniversityException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturers in " + exception.getUniversityName() + " university not found");
    }

    @ExceptionHandler(LecturersNotFoundByRoleException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturersNotFoundByRole(LecturersNotFoundByRoleException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturers with " + exception.getRoleName() + " role not found");
    }

    @ExceptionHandler(LecturersNotFoundByDegreeException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturersNotFoundByDegree(LecturersNotFoundByDegreeException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturers with " + exception.getDegreeName() + " degree not found");
    }

    @ExceptionHandler(LecturerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturerNotFound(LecturerNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturer with " + exception.getLecturerId() + " id not found");
    }

    @ExceptionHandler(LecturersNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error lecturersNotFound(LecturersNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Lecturers not found");
    }

    @ExceptionHandler(StudentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentNotFound(StudentNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Student with " + exception.getStudentId() + " id not found");
    }

    @ExceptionHandler(StudentsNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentsNotFound(StudentsNotFoundException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Students not found");
    }

    @ExceptionHandler(StudentsNotFoundInFacultyException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentsNotFoundInFaculty(StudentsNotFoundInFacultyException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Students in " + exception.getFacultyName() + " faculty not found");
    }

    @ExceptionHandler(StudentsNotFoundInUniversityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentsNotFoundInUniversity(StudentsNotFoundInUniversityException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Students in " + exception.getUniversityName() + " university not found");
    }

    @ExceptionHandler(StudentsNotFoundByRoleException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentsNotFoundByRole(StudentsNotFoundByRoleException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Students with " + exception.getRoleName() + " role not found");
    }

    @ExceptionHandler(StudentsNotFoundByDegreeException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error studentsNotFoundByDegree(StudentsNotFoundByDegreeException exception) {
        return new Error(HttpStatus.NOT_FOUND.value(), "Students with " + exception.getDegreeName() + " degree not found");
    }


}
