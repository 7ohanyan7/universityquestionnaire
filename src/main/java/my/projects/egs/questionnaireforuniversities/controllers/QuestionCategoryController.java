package my.projects.egs.questionnaireforuniversities.controllers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCategoryDTO;
import my.projects.egs.questionnaireforuniversities.exceptions.SaveException;
import my.projects.egs.questionnaireforuniversities.exceptions.categoryreleated.CategoriesNotFoundException;
import my.projects.egs.questionnaireforuniversities.exceptions.categoryreleated.CategoryNotFoundException;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionCategoryService;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionCategoryController {

    private QuestionCategoryService questionCategoryService;

    public QuestionCategoryController(QuestionCategoryService questionCategoryService) {
        this.questionCategoryService = questionCategoryService;
    }

    @PostMapping(value = "/categories")
    public QuestionCategoryDTO saveCategory(@Valid @RequestBody QuestionCategoryDTO questionCategoryDTO, Errors errors) {
        if (errors.hasErrors()) {
            throw new SaveException(errors);
        }
        return questionCategoryService.save(questionCategoryDTO);
    }

    @GetMapping(value = "/categories")
    public List<QuestionCategoryDTO> getAllCategories() {
        List<QuestionCategoryDTO> categories = questionCategoryService.findAll();
        if (!categories.isEmpty()) {
            return categories;
        }
        throw new CategoriesNotFoundException();
    }

    @PutMapping(value = "/categories/{id}")
    public QuestionCategoryDTO updateCategory(@PathVariable int id, @RequestBody QuestionCategoryDTO questionCategoryDTO) {
        if (questionCategoryService.findByID(id) != null) {
            questionCategoryDTO.setId(id);
            return questionCategoryService.save(questionCategoryDTO);
        }
        throw new CategoryNotFoundException(id);
    }

    @DeleteMapping(value = "/categories/{id}")
    public void deleteCategory(@PathVariable int id) {
        if (questionCategoryService.findByID(id) != null) {
            questionCategoryService.deleteById(id);
        }else throw new CategoryNotFoundException(id);
    }
}
