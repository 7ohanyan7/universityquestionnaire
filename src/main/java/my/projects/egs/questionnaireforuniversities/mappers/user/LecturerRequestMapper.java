package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.entities.Lecturer;
import my.projects.egs.questionnaireforuniversities.mappers.Mapper;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class LecturerRequestMapper implements Mapper<Lecturer, LecturerRequestDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public LecturerRequestMapper(UniversityRepository universityRepository,
                                 FacultyRepository facultyRepository, DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public LecturerRequestDTO entityToDto(Lecturer lecturer) {
        if (lecturer != null) {
            LecturerRequestDTO lecturerRequestDTO = new LecturerRequestDTO();

            lecturerRequestDTO.setId(lecturer.getId());
            lecturerRequestDTO.setCreatedAt(lecturer.getCreatedAt());
            lecturerRequestDTO.setUpdatedAt(lecturer.getUpdatedAt());
            lecturerRequestDTO.setFirstName(lecturer.getFirstName());
            lecturerRequestDTO.setLastName(lecturer.getLastName());
            lecturerRequestDTO.setLogin(lecturer.getLogin());
            lecturerRequestDTO.setPasswordHash(lecturer.getPasswordHash());
            lecturerRequestDTO.setPasswordSalt(lecturer.getPasswordSalt());
            lecturerRequestDTO.setBirthDate(lecturer.getBirthDate());
            lecturerRequestDTO.setCountry(lecturer.getCountry());
            lecturerRequestDTO.setCity(lecturer.getCity());
            lecturerRequestDTO.setRegion(lecturer.getRegion());
            lecturerRequestDTO.setStreet(lecturer.getStreet());
            lecturerRequestDTO.setEmail(lecturer.getEmail());
            lecturerRequestDTO.setPhone(lecturer.getPhone());
            lecturerRequestDTO.setUserType(lecturer.getUserType());
            lecturerRequestDTO.setUniversityName(lecturer.getUniversity().getUniversityName());
            lecturerRequestDTO.setFacultyName(lecturer.getFaculty().getFacultyName());
            lecturerRequestDTO.setDegreeName(lecturer.getDegree().getDegreeName());
            lecturerRequestDTO.setRoleName(lecturer.getRole().getRoleName());

            return lecturerRequestDTO;
        }

        return null;
    }

    @Override
    public Lecturer dtoToEntity(LecturerRequestDTO lecturerRequestDTO) {

        if (lecturerRequestDTO != null) {
            Lecturer lecturer = new Lecturer();

            lecturer.setId(lecturerRequestDTO.getId());
            lecturer.setCreatedAt(lecturerRequestDTO.getCreatedAt());
            lecturer.setUpdatedAt(lecturerRequestDTO.getUpdatedAt());
            lecturer.setFirstName(lecturerRequestDTO.getFirstName());
            lecturer.setLastName(lecturerRequestDTO.getLastName());
            lecturer.setLogin(lecturerRequestDTO.getLogin());
            lecturer.setPasswordHash(lecturerRequestDTO.getPasswordHash());
            lecturer.setPasswordSalt(lecturerRequestDTO.getPasswordSalt());
            lecturer.setBirthDate(lecturerRequestDTO.getBirthDate());
            lecturer.setCountry(lecturerRequestDTO.getCountry());
            lecturer.setCity(lecturerRequestDTO.getCity());
            lecturer.setRegion(lecturerRequestDTO.getRegion());
            lecturer.setStreet(lecturerRequestDTO.getStreet());
            lecturer.setEmail(lecturerRequestDTO.getEmail());
            lecturer.setPhone(lecturerRequestDTO.getPhone());
            lecturer.setUserType(lecturerRequestDTO.getUserType());
            lecturer.setUniversity(universityRepository.findByUniversityName(lecturerRequestDTO.getUniversityName()).get());
            lecturer.setFaculty(facultyRepository.findByFacultyName(lecturerRequestDTO.getFacultyName()).get());
            lecturer.setDegree(degreeRepository.findByDegreeName(lecturerRequestDTO.getDegreeName()).get());
            lecturer.setRole(roleRepository.findByRoleName(lecturerRequestDTO.getRoleName()).get());

            return lecturer;
        }

        return null;

    }

    @Override
    public List<LecturerRequestDTO> entitiesToDtos(List<Lecturer> lecturers) {
        if (lecturers != null) {
            List<LecturerRequestDTO> lecturerRequestDTOS = new ArrayList<>();

            for (Lecturer lecturer : lecturers) {
                lecturerRequestDTOS.add(entityToDto(lecturer));
            }

            return lecturerRequestDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Lecturer> dtosToEntities(List<LecturerRequestDTO> lecturerRequestDTOS) {
        if (lecturerRequestDTOS != null) {
            List<Lecturer> lecturers = new ArrayList<>();

            for (LecturerRequestDTO lecturerRequestDTO : lecturerRequestDTOS) {
                lecturers.add(dtoToEntity(lecturerRequestDTO));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }
}
