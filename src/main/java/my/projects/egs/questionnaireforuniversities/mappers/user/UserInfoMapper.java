package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.UserInfoDTO;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.stereotype.Component;

@Component
public class UserInfoMapper {

    public UserInfoDTO entityToDto(User user) {

        if (user != null) {
            UserInfoDTO userInfoDTO = new UserInfoDTO();

            userInfoDTO.setId(user.getId());
            userInfoDTO.setFirstName(user.getFirstName());
            userInfoDTO.setLastName(user.getLastName());
            userInfoDTO.setLogin(user.getLogin());
            userInfoDTO.setUserType(user.getUserType());

            return userInfoDTO;
        }

        return null;
    }


}
