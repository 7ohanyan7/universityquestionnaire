package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.mappers.*;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class UserResponseMapper implements Mapper<User, UserResponseDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserResponseMapper(UniversityRepository universityRepository,
                             FacultyRepository facultyRepository, DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }


    @Override
    public UserResponseDTO entityToDto(User user) {

        if (user != null) {
            UserResponseDTO userResponseDTO = new UserResponseDTO();
            
            userResponseDTO.setId(user.getId());
            userResponseDTO.setCreatedAt(user.getCreatedAt());
            userResponseDTO.setUpdatedAt(user.getUpdatedAt());
            userResponseDTO.setFirstName(user.getFirstName());
            userResponseDTO.setLastName(user.getLastName());
            userResponseDTO.setLogin(user.getLogin());
            userResponseDTO.setBirthDate(user.getBirthDate());
            userResponseDTO.setCountry(user.getCountry());
            userResponseDTO.setCity(user.getCity());
            userResponseDTO.setRegion(user.getRegion());
            userResponseDTO.setStreet(user.getStreet());
            userResponseDTO.setEmail(user.getEmail());
            userResponseDTO.setPhone(user.getPhone());
            userResponseDTO.setUserType(user.getUserType());
            userResponseDTO.setUniversityName(user.getUniversity().getUniversityName());
            userResponseDTO.setFacultyName(user.getFaculty().getFacultyName());
            userResponseDTO.setDegreeName(user.getDegree().getDegreeName());
            userResponseDTO.setRoleName(user.getRole().getRoleName());
            
            return userResponseDTO;
        }
        
        return null;
    }

    @Override
    public User dtoToEntity(UserResponseDTO userResponseDTO) {
        
        if (userResponseDTO != null) {
            User user = new User();

            user.setId(userResponseDTO.getId());
            user.setCreatedAt(userResponseDTO.getCreatedAt());
            user.setUpdatedAt(userResponseDTO.getUpdatedAt());
            user.setFirstName(userResponseDTO.getFirstName());
            user.setLastName(userResponseDTO.getLastName());
            user.setLogin(userResponseDTO.getLogin());
            user.setBirthDate(userResponseDTO.getBirthDate());
            user.setCountry(userResponseDTO.getCountry());
            user.setCity(userResponseDTO.getCity());
            user.setRegion(userResponseDTO.getRegion());
            user.setStreet(userResponseDTO.getStreet());
            user.setEmail(userResponseDTO.getEmail());
            user.setPhone(userResponseDTO.getPhone());
            user.setUserType(userResponseDTO.getUserType());
            user.setUniversity(universityRepository.findByUniversityName(userResponseDTO.getUniversityName()).get());
            user.setFaculty(facultyRepository.findByFacultyName(userResponseDTO.getFacultyName()).get());
            user.setDegree(degreeRepository.findByDegreeName(userResponseDTO.getDegreeName()).get());
            user.setRole(roleRepository.findByRoleName(userResponseDTO.getRoleName()).get());

            return user;
        }

        return null;
    }

    @Override
    public List<UserResponseDTO> entitiesToDtos(List<User> users) {

        if (users != null) {
            List<UserResponseDTO> userResponseDTOS = new ArrayList<>();

            for (User user : users) {
                userResponseDTOS.add(entityToDto(user));
            }

            return userResponseDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<User> dtosToEntities(List<UserResponseDTO> userResponseDTOS) {

        if (userResponseDTOS != null) {
            List<User> users = new ArrayList<>();

            for (UserResponseDTO userResponseDTO : userResponseDTOS) {
                users.add(dtoToEntity(userResponseDTO));
            }

            return users;
        }

        return Collections.emptyList();
    }
}
