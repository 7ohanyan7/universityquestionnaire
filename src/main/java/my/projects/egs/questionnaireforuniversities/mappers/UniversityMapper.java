package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;
import my.projects.egs.questionnaireforuniversities.entities.University;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserResponseMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UniversityMapper implements Mapper<University, UniversityDTO> {

    private UserResponseMapper userResponseMapper;
    private FacultyMapper facultyMapper;

    public UniversityMapper(UserResponseMapper userResponseMapper, FacultyMapper facultyMapper) {
        this.userResponseMapper = userResponseMapper;
        this.facultyMapper = facultyMapper;
    }

    @Override
    public UniversityDTO entityToDto(University university) {
        if (university != null) {
            UniversityDTO universityDTO = new UniversityDTO();

            universityDTO.setCreatedAt(university.getCreatedAt());
            universityDTO.setUpdatedAt(university.getUpdatedAt());
            universityDTO.setId(university.getId());
            universityDTO.setUniversityName(university.getUniversityName());
            universityDTO.setUserResponseDTOS(userResponseMapper.entitiesToDtos(university.getUsers()));
            universityDTO.setFacultyDTOS(facultyMapper.entitiesToDtos(university.getFaculties()));

            return universityDTO;
        }

        return null;
    }

    @Override
    public University dtoToEntity(UniversityDTO universityDTO) {
        if (universityDTO != null) {
            University university = new University();

            university.setCreatedAt(universityDTO.getCreatedAt());
            university.setUpdatedAt(universityDTO.getUpdatedAt());
            university.setId(universityDTO.getId());
            university.setUniversityName(universityDTO.getUniversityName());
            university.setUsers(userResponseMapper.dtosToEntities(universityDTO.getUserResponseDTOS()));
            university.setFaculties(facultyMapper.dtosToEntities(universityDTO.getFacultyDTOS()));

            return university;
        }

        return null;
    }

    @Override
    public List<UniversityDTO> entitiesToDtos(List<University> universities) {

        if (universities != null) {
            List<UniversityDTO> universityDTOS = new ArrayList<>();

            for (University university : universities) {
                universityDTOS.add(entityToDto(university));
            }

            return universityDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<University> dtosToEntities(List<UniversityDTO> universityDTOS) {

        if (universityDTOS != null) {
            List<University> universities = new ArrayList<>();

            for (UniversityDTO universityDTO : universityDTOS) {
                universities.add(dtoToEntity(universityDTO));
            }

            return universities;
        }

        return Collections.emptyList();
    }
}
