package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.RoleDTO;
import my.projects.egs.questionnaireforuniversities.entities.Role;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserResponseMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class RoleMapper implements Mapper<Role, RoleDTO> {

    private UserResponseMapper userResponseMapper;

    public RoleMapper(UserResponseMapper userResponseMapper) {
        this.userResponseMapper = userResponseMapper;
    }

    @Override
    public RoleDTO entityToDto(Role role) {
        if (role != null) {
            RoleDTO roleDTO = new RoleDTO();

            roleDTO.setCreatedAt(role.getCreatedAt());
            roleDTO.setUpdatedAt(role.getUpdatedAt());
            roleDTO.setId(role.getId());
            roleDTO.setRoleName(role.getRoleName());
            roleDTO.setUserResponseDTOS(userResponseMapper.entitiesToDtos(role.getUsers()));

            return roleDTO;
        }

        return null;
    }

    @Override
    public Role dtoToEntity(RoleDTO roleDTO) {
        if (roleDTO != null) {
            Role role = new Role();

            role.setCreatedAt(roleDTO.getCreatedAt());
            role.setUpdatedAt(roleDTO.getUpdatedAt());
            role.setId(roleDTO.getId());
            role.setRoleName(roleDTO.getRoleName());
            role.setUsers(userResponseMapper.dtosToEntities(roleDTO.getUserResponseDTOS()));

            return role;
        }

        return null;
    }

    @Override
    public List<RoleDTO> entitiesToDtos(List<Role> roles) {
        if (roles != null) {
            List<RoleDTO> roleDTOS = new ArrayList<>();

            for (Role role : roles) {
                roleDTOS.add(entityToDto(role));
            }

            return roleDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Role> dtosToEntities(List<RoleDTO> roleDTOS) {
        if (roleDTOS != null) {
            List<Role> roles = new ArrayList<>();

            for (RoleDTO roleDTO : roleDTOS) {
                roles.add(dtoToEntity(roleDTO));
            }

            return roles;
        }

        return Collections.emptyList();
    }
}
