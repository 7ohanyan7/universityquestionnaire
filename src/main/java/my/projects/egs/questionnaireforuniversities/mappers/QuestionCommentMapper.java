package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCommentDTO;
import my.projects.egs.questionnaireforuniversities.entities.QuestionComment;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserInfoMapper;
import my.projects.egs.questionnaireforuniversities.repositories.LecturerRepository;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionRepository;
import my.projects.egs.questionnaireforuniversities.repositories.StudentRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class QuestionCommentMapper implements Mapper<QuestionComment, QuestionCommentDTO> {

    private QuestionRepository questionRepository;
    private UserInfoMapper userInfoMapper;
    private UserRepository userRepository;

    public QuestionCommentMapper(QuestionRepository questionRepository, UserInfoMapper userInfoMapper,
                                 UserRepository userRepository) {
        this.questionRepository = questionRepository;
        this.userInfoMapper = userInfoMapper;
        this.userRepository = userRepository;
    }


    @Override
    public QuestionCommentDTO entityToDto(QuestionComment questionComment) {
        if (questionComment != null) {
            QuestionCommentDTO questionCommentDTO = new QuestionCommentDTO();

            questionCommentDTO.setCreatedAt(questionComment.getCreatedAt());
            questionCommentDTO.setUpdatedAt(questionComment.getUpdatedAt());
            questionCommentDTO.setId(questionComment.getId());
            questionCommentDTO.setCommentText(questionComment.getCommentText());
            questionCommentDTO.setQuestionID(questionComment.getQuestion().getId());
            questionCommentDTO.setUserInfoDTO(userInfoMapper.entityToDto(questionComment.getUser()));

            return questionCommentDTO;
        }

        return null;
    }

    @Override
    public QuestionComment dtoToEntity(QuestionCommentDTO questionCommentDTO) {
        if (questionCommentDTO != null) {
            QuestionComment questionComment = new QuestionComment();

            questionComment.setCreatedAt(questionCommentDTO.getCreatedAt());
            questionComment.setUpdatedAt(questionCommentDTO.getUpdatedAt());
            questionComment.setId(questionCommentDTO.getId());
            questionComment.setCommentText(questionCommentDTO.getCommentText());
            questionComment.setQuestion(questionRepository.findById(questionCommentDTO.getId()).get());
            questionComment.setUser(userRepository.findById(questionCommentDTO.getUserInfoDTO().getId()).get());

            return questionComment;
        }

        return null;
    }

    @Override
    public List<QuestionCommentDTO> entitiesToDtos(List<QuestionComment> questionComments) {
        if (questionComments != null) {
            List<QuestionCommentDTO> questionCommentDTOS = new ArrayList<>();

            for (QuestionComment questionComment : questionComments) {
                questionCommentDTOS.add(entityToDto(questionComment));
            }

            return questionCommentDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<QuestionComment> dtosToEntities(List<QuestionCommentDTO> questionCommentDTOS) {
        if (questionCommentDTOS != null) {
            List<QuestionComment> questionComments = new ArrayList<>();

            for (QuestionCommentDTO questionCommentDTO : questionCommentDTOS) {
                questionComments.add(dtoToEntity(questionCommentDTO));
            }

            return questionComments;
        }

        return Collections.emptyList();
    }

}
