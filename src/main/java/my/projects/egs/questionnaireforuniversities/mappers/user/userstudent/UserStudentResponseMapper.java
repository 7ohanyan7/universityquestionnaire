package my.projects.egs.questionnaireforuniversities.mappers.user.userstudent;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.StudentResponseDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.Student;
import my.projects.egs.questionnaireforuniversities.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UserStudentResponseMapper {
    public Student userToStudent(User user) {

        if (user != null) {
            Student student = new Student();

            student.setId(user.getId());
            student.setCreatedAt(user.getCreatedAt());
            student.setUpdatedAt(user.getUpdatedAt());
            student.setFirstName(user.getFirstName());
            student.setLastName(user.getLastName());
            student.setLogin(user.getLogin());
            student.setPasswordHash(user.getPasswordHash());
            student.setPasswordSalt(user.getPasswordSalt());
            student.setBirthDate(user.getBirthDate());
            student.setCountry(user.getCountry());
            student.setCity(user.getCity());
            student.setRegion(user.getRegion());
            student.setStreet(user.getStreet());
            student.setEmail(user.getEmail());
            student.setPhone(user.getPhone());
            student.setUniversity(user.getUniversity());
            student.setFaculty(user.getFaculty());
            student.setDegree(user.getDegree());
            student.setRole(user.getRole());
            student.setQuestions(user.getQuestions());
            student.setAnswers(user.getAnswers());
            student.setQuestionComments(user.getQuestionComments());
            student.setAnswerComments(user.getAnswerComments());
            student.setUserType(user.getUserType());

            return student;
        }

        return null;
    }

    public StudentResponseDTO userResponseDtoToStudentResponseDto(UserResponseDTO userResponseDTO) {
        if (userResponseDTO != null) {
            StudentResponseDTO studentResponseDTO = new StudentResponseDTO();

            studentResponseDTO.setId(userResponseDTO.getId());
            studentResponseDTO.setCreatedAt(userResponseDTO.getCreatedAt());
            studentResponseDTO.setUpdatedAt(userResponseDTO.getUpdatedAt());
            studentResponseDTO.setFirstName(userResponseDTO.getFirstName());
            studentResponseDTO.setLastName(userResponseDTO.getLastName());
            studentResponseDTO.setLogin(userResponseDTO.getLogin());
            studentResponseDTO.setBirthDate(userResponseDTO.getBirthDate());
            studentResponseDTO.setCountry(userResponseDTO.getCountry());
            studentResponseDTO.setCity(userResponseDTO.getCity());
            studentResponseDTO.setRegion(userResponseDTO.getRegion());
            studentResponseDTO.setStreet(userResponseDTO.getStreet());
            studentResponseDTO.setEmail(userResponseDTO.getEmail());
            studentResponseDTO.setPhone(userResponseDTO.getPhone());
            studentResponseDTO.setUniversityName(userResponseDTO.getUniversityName());
            studentResponseDTO.setFacultyName(userResponseDTO.getFacultyName());
            studentResponseDTO.setDegreeName(userResponseDTO.getDegreeName());
            studentResponseDTO.setRoleName(userResponseDTO.getRoleName());
            studentResponseDTO.setUserType(userResponseDTO.getUserType());

            return studentResponseDTO;
        }

        return null;
    }

    public List<Student> usersToStudents(List<User> users) {
        if (users != null) {
            List<Student> lecturers = new ArrayList<>();

            for (User user : users) {
                lecturers.add(userToStudent(user));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }

    public List<StudentResponseDTO> userResponseDtosToStudentResponseDtos(List<UserResponseDTO> userResponseDTOS) {
        if (userResponseDTOS != null) {
            List<StudentResponseDTO> studentResponseDTOS = new ArrayList<>();

            for (UserResponseDTO userResponseDTO : userResponseDTOS) {
                studentResponseDTOS.add(userResponseDtoToStudentResponseDto(userResponseDTO));
            }

            return studentResponseDTOS;
        }

        return Collections.emptyList();
    }
}
