package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.entities.Answer;

import java.util.List;
import java.util.Optional;

public interface Mapper<EntityType, DtoType> {

    DtoType entityToDto(EntityType entityType);

    EntityType dtoToEntity(DtoType dtoType);

    List<DtoType> entitiesToDtos(List<EntityType> entityTypes);

    List<EntityType> dtosToEntities(List<DtoType> dtoTypes);

}
