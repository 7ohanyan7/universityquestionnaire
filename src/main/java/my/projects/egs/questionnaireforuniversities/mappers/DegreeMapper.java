package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.DegreeDTO;
import my.projects.egs.questionnaireforuniversities.entities.Degree;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserResponseMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class DegreeMapper implements Mapper<Degree, DegreeDTO> {

    private UserResponseMapper userResponseMapper;

    public DegreeMapper(UserResponseMapper userResponseMapper) {
        this.userResponseMapper = userResponseMapper;
    }

    @Override
    public DegreeDTO entityToDto(Degree degree) {
        if (degree != null) {
            DegreeDTO degreeDTO = new DegreeDTO();

            degreeDTO.setCreatedAt(degree.getCreatedAt());
            degreeDTO.setUpdatedAt(degree.getUpdatedAt());
            degreeDTO.setId(degree.getId());
            degreeDTO.setDegreeName(degree.getDegreeName());
            degreeDTO.setUserResponseDTOS(userResponseMapper.entitiesToDtos(degree.getUsers()));

            return degreeDTO;
        }

        return null;
    }

    @Override
    public Degree dtoToEntity(DegreeDTO degreeDTO) {
        if (degreeDTO != null) {
            Degree degree = new Degree();

            degree.setCreatedAt(degreeDTO.getCreatedAt());
            degree.setUpdatedAt(degreeDTO.getUpdatedAt());
            degree.setId(degreeDTO.getId());
            degree.setDegreeName(degreeDTO.getDegreeName());
            degree.setUsers(userResponseMapper.dtosToEntities(degreeDTO.getUserResponseDTOS()));

            return degree;
        }

        return null;
    }

    @Override
    public List<DegreeDTO> entitiesToDtos(List<Degree> degrees) {
        if (degrees != null) {
            List<DegreeDTO> degreeDTOS = new ArrayList<>();

            for (Degree degree : degrees) {
                degreeDTOS.add(entityToDto(degree));
            }

            return degreeDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Degree> dtosToEntities(List<DegreeDTO> degreeDTOS) {
        if (degreeDTOS != null) {
            List<Degree> degrees = new ArrayList<>();

            for (DegreeDTO degreeDTO : degreeDTOS) {
                degrees.add(dtoToEntity(degreeDTO));
            }

            return degrees;
        }

        return Collections.emptyList();
    }
}
