package my.projects.egs.questionnaireforuniversities.mappers.user.userlecturer;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.UserRequestDTO;
import my.projects.egs.questionnaireforuniversities.entities.Lecturer;
import my.projects.egs.questionnaireforuniversities.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UserLecturerRequestMapper {

    public Lecturer userToLecturer(User user) {

        if (user != null) {
            Lecturer lecturer = new Lecturer();

            lecturer.setId(user.getId());
            lecturer.setCreatedAt(user.getCreatedAt());
            lecturer.setUpdatedAt(user.getUpdatedAt());
            lecturer.setFirstName(user.getFirstName());
            lecturer.setLastName(user.getLastName());
            lecturer.setLogin(user.getLogin());
            lecturer.setPasswordHash(user.getPasswordHash());
            lecturer.setPasswordSalt(user.getPasswordSalt());
            lecturer.setBirthDate(user.getBirthDate());
            lecturer.setCountry(user.getCountry());
            lecturer.setCity(user.getCity());
            lecturer.setRegion(user.getRegion());
            lecturer.setStreet(user.getStreet());
            lecturer.setEmail(user.getEmail());
            lecturer.setPhone(user.getPhone());
            lecturer.setUniversity(user.getUniversity());
            lecturer.setFaculty(user.getFaculty());
            lecturer.setDegree(user.getDegree());
            lecturer.setRole(user.getRole());
            lecturer.setQuestions(user.getQuestions());
            lecturer.setAnswers(user.getAnswers());
            lecturer.setQuestionComments(user.getQuestionComments());
            lecturer.setAnswerComments(user.getAnswerComments());
            lecturer.setUserType(user.getUserType());

            return lecturer;
        }

        return null;
    }

    public LecturerRequestDTO userRequestDtoToLecturerRequestDto(UserRequestDTO userRequestDTO) {
        if (userRequestDTO != null) {
            LecturerRequestDTO lecturerRequestDTO = new LecturerRequestDTO();

            lecturerRequestDTO.setId(userRequestDTO.getId());
            lecturerRequestDTO.setCreatedAt(userRequestDTO.getCreatedAt());
            lecturerRequestDTO.setUpdatedAt(userRequestDTO.getUpdatedAt());
            lecturerRequestDTO.setFirstName(userRequestDTO.getFirstName());
            lecturerRequestDTO.setLastName(userRequestDTO.getLastName());
            lecturerRequestDTO.setLogin(userRequestDTO.getLogin());
            lecturerRequestDTO.setPasswordHash(userRequestDTO.getPasswordHash());
            lecturerRequestDTO.setPasswordSalt(userRequestDTO.getPasswordSalt());
            lecturerRequestDTO.setBirthDate(userRequestDTO.getBirthDate());
            lecturerRequestDTO.setCountry(userRequestDTO.getCountry());
            lecturerRequestDTO.setCity(userRequestDTO.getCity());
            lecturerRequestDTO.setRegion(userRequestDTO.getRegion());
            lecturerRequestDTO.setStreet(userRequestDTO.getStreet());
            lecturerRequestDTO.setEmail(userRequestDTO.getEmail());
            lecturerRequestDTO.setPhone(userRequestDTO.getPhone());
            lecturerRequestDTO.setUniversityName(userRequestDTO.getUniversityName());
            lecturerRequestDTO.setFacultyName(userRequestDTO.getFacultyName());
            lecturerRequestDTO.setDegreeName(userRequestDTO.getDegreeName());
            lecturerRequestDTO.setRoleName(userRequestDTO.getRoleName());
            lecturerRequestDTO.setUserType(userRequestDTO.getUserType());
            return lecturerRequestDTO;
        }

        return null;
    }

    public List<Lecturer> usersToLecturers(List<User> users) {
        if (users != null) {
            List<Lecturer> lecturers = new ArrayList<>();

            for (User user : users) {
                lecturers.add(userToLecturer(user));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }

    public List<LecturerRequestDTO> userRequestDtosToLecturerRequestDtos(List<UserRequestDTO> userRequestDTOS) {
        if (userRequestDTOS != null) {
            List<LecturerRequestDTO> lecturerRequestDTOS = new ArrayList<>();

            for (UserRequestDTO userRequestDTO : userRequestDTOS) {
                lecturerRequestDTOS.add(userRequestDtoToLecturerRequestDto(userRequestDTO));
            }

            return lecturerRequestDTOS;
        }

        return Collections.emptyList();
    }

}
