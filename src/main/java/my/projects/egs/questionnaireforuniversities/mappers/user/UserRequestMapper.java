package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.UserRequestDTO;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.mappers.Mapper;
import my.projects.egs.questionnaireforuniversities.mappers.QuestionMapper;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UserRequestMapper implements Mapper<User, UserRequestDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserRequestMapper(UniversityRepository universityRepository, FacultyRepository facultyRepository,
                             DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserRequestDTO entityToDto(User user) {

        if (user != null) {
            UserRequestDTO userRequestDTO = new UserRequestDTO();

            userRequestDTO.setId(user.getId());
            userRequestDTO.setCreatedAt(user.getCreatedAt());
            userRequestDTO.setUpdatedAt(user.getUpdatedAt());
            userRequestDTO.setFirstName(user.getFirstName());
            userRequestDTO.setLastName(user.getLastName());
            userRequestDTO.setLogin(user.getLogin());
            userRequestDTO.setPasswordHash(user.getPasswordHash());
            userRequestDTO.setPasswordSalt(user.getPasswordSalt());
            userRequestDTO.setBirthDate(user.getBirthDate());
            userRequestDTO.setCountry(user.getCountry());
            userRequestDTO.setCity(user.getCity());
            userRequestDTO.setRegion(user.getRegion());
            userRequestDTO.setStreet(user.getStreet());
            userRequestDTO.setEmail(user.getEmail());
            userRequestDTO.setPhone(user.getPhone());
            userRequestDTO.setUserType(user.getUserType());
            userRequestDTO.setUniversityName(user.getUniversity().getUniversityName());
            userRequestDTO.setFacultyName(user.getFaculty().getFacultyName());
            userRequestDTO.setDegreeName(user.getDegree().getDegreeName());
            userRequestDTO.setRoleName(user.getRole().getRoleName());

            return userRequestDTO;
        }

        return null;
    }

    @Override
    public User dtoToEntity(UserRequestDTO userRequestDTO) {

        if (userRequestDTO != null) {
            User user = new User();

            user.setId(userRequestDTO.getId());
            user.setCreatedAt(userRequestDTO.getCreatedAt());
            user.setUpdatedAt(userRequestDTO.getUpdatedAt());
            user.setFirstName(userRequestDTO.getFirstName());
            user.setLastName(userRequestDTO.getLastName());
            user.setLogin(userRequestDTO.getLogin());
            user.setPasswordHash(userRequestDTO.getPasswordHash());
            user.setPasswordSalt(userRequestDTO.getPasswordSalt());
            user.setBirthDate(userRequestDTO.getBirthDate());
            user.setCountry(userRequestDTO.getCountry());
            user.setCity(userRequestDTO.getCity());
            user.setRegion(userRequestDTO.getRegion());
            user.setStreet(userRequestDTO.getStreet());
            user.setEmail(userRequestDTO.getEmail());
            user.setPhone(userRequestDTO.getPhone());
            user.setUserType(userRequestDTO.getUserType());
            user.setUniversity(universityRepository.findByUniversityName(userRequestDTO.getUniversityName()).get());
            user.setFaculty(facultyRepository.findByFacultyName(userRequestDTO.getFacultyName()).get());
            user.setDegree(degreeRepository.findByDegreeName(userRequestDTO.getDegreeName()).get());
            user.setRole(roleRepository.findByRoleName(userRequestDTO.getRoleName()).get());

            return user;
        }

        return null;
    }

    @Override
    public List<UserRequestDTO> entitiesToDtos(List<User> users) {
        if (users != null) {
            List<UserRequestDTO> userRequestDTOS = new ArrayList<>();

            for (User user : users) {
                userRequestDTOS.add(entityToDto(user));
            }

            return userRequestDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<User> dtosToEntities(List<UserRequestDTO> userRequestDTOS) {

        if (userRequestDTOS != null) {
            List<User> users = new ArrayList<>();

            for (UserRequestDTO userRequestDTO : userRequestDTOS) {
                users.add(dtoToEntity(userRequestDTO));
            }

            return users;
        }

        return Collections.emptyList();
    }
}
