package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.entities.Student;
import my.projects.egs.questionnaireforuniversities.mappers.Mapper;
import my.projects.egs.questionnaireforuniversities.repositories.*;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class StudentRequestMapper implements Mapper<Student, StudentRequestDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public StudentRequestMapper(UniversityRepository universityRepository,
                             FacultyRepository facultyRepository, DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public StudentRequestDTO entityToDto(Student student) {
        if (student != null) {
            StudentRequestDTO studentRequestDTO = new StudentRequestDTO();

            studentRequestDTO.setId(student.getId());
            studentRequestDTO.setCreatedAt(student.getCreatedAt());
            studentRequestDTO.setUpdatedAt(student.getUpdatedAt());
            studentRequestDTO.setFirstName(student.getFirstName());
            studentRequestDTO.setLastName(student.getLastName());
            studentRequestDTO.setLogin(student.getLogin());
            studentRequestDTO.setPasswordHash(student.getPasswordHash());
            studentRequestDTO.setPasswordSalt(student.getPasswordSalt());
            studentRequestDTO.setBirthDate(student.getBirthDate());
            studentRequestDTO.setCountry(student.getCountry());
            studentRequestDTO.setCity(student.getCity());
            studentRequestDTO.setRegion(student.getRegion());
            studentRequestDTO.setStreet(student.getStreet());
            studentRequestDTO.setEmail(student.getEmail());
            studentRequestDTO.setPhone(student.getPhone());
            studentRequestDTO.setUserType(student.getUserType());
            studentRequestDTO.setUniversityName(student.getUniversity().getUniversityName());
            studentRequestDTO.setFacultyName(student.getFaculty().getFacultyName());
            studentRequestDTO.setDegreeName(student.getDegree().getDegreeName());
            studentRequestDTO.setCourse(student.getCourse());
            studentRequestDTO.setRoleName(student.getRole().getRoleName());

            return studentRequestDTO;
        }

        return  null;
    }

    @Override
    public Student dtoToEntity(StudentRequestDTO studentRequestDTO) {

        if (studentRequestDTO != null) {
            Student student = new Student();

            student.setId(studentRequestDTO.getId());
            student.setCreatedAt(studentRequestDTO.getCreatedAt());
            student.setUpdatedAt(studentRequestDTO.getUpdatedAt());
            student.setFirstName(studentRequestDTO.getFirstName());
            student.setLastName(studentRequestDTO.getLastName());
            student.setLogin(studentRequestDTO.getLogin());
            student.setPasswordHash(studentRequestDTO.getPasswordHash());
            student.setPasswordSalt(studentRequestDTO.getPasswordSalt());
            student.setBirthDate(studentRequestDTO.getBirthDate());
            student.setCountry(studentRequestDTO.getCountry());
            student.setCity(studentRequestDTO.getCity());
            student.setRegion(studentRequestDTO.getRegion());
            student.setStreet(studentRequestDTO.getStreet());
            student.setEmail(studentRequestDTO.getEmail());
            student.setPhone(studentRequestDTO.getPhone());
            student.setUserType(studentRequestDTO.getUserType());
            student.setUniversity(universityRepository.findByUniversityName(studentRequestDTO.getUniversityName()).get());
            student.setFaculty(facultyRepository.findByFacultyName(studentRequestDTO.getFacultyName()).get());
            student.setDegree(degreeRepository.findByDegreeName(studentRequestDTO.getDegreeName()).get());
            student.setCourse(studentRequestDTO.getCourse());
            student.setRole(roleRepository.findByRoleName(studentRequestDTO.getRoleName()).get());

            return student;
        }

        return null;
    }

    @Override
    public List<StudentRequestDTO> entitiesToDtos(List<Student> students) {
        if (students != null) {
            List<StudentRequestDTO> studentRequestDTOS = new ArrayList<>();

            for (Student student : students) {
                studentRequestDTOS.add(entityToDto(student));
            }

            return studentRequestDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Student> dtosToEntities(List<StudentRequestDTO> studentRequestDTOS) {
        if (studentRequestDTOS != null) {
            List<Student> students = new ArrayList<>();

            for (StudentRequestDTO studentRequestDTO : studentRequestDTOS) {
                students.add(dtoToEntity(studentRequestDTO));
            }

            return students;
        }

        return Collections.emptyList();
    }
}
