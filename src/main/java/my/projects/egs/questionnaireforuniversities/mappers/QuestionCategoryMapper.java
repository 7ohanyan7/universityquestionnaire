package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCategoryDTO;
import my.projects.egs.questionnaireforuniversities.entities.QuestionCategory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class QuestionCategoryMapper implements Mapper<QuestionCategory, QuestionCategoryDTO> {

    private QuestionMapper questionMapper;

    public QuestionCategoryMapper(QuestionMapper questionMapper) {
        this.questionMapper = questionMapper;
    }

    @Override
    public QuestionCategoryDTO entityToDto(QuestionCategory questionCategory) {
        if (questionCategory != null) {
            QuestionCategoryDTO questionCategoryDTO = new QuestionCategoryDTO();

            questionCategoryDTO.setCreatedAt(questionCategory.getCreatedAt());
            questionCategoryDTO.setUpdatedAt(questionCategory.getUpdatedAt());
            questionCategoryDTO.setId(questionCategory.getId());
            questionCategoryDTO.setCategoryName(questionCategory.getCategoryName());
            questionCategoryDTO.setQuestionDTOS(questionMapper.entitiesToDtos(questionCategory.getQuestions()));

            return questionCategoryDTO;
        }

        return null;
    }

    @Override
    public QuestionCategory dtoToEntity(QuestionCategoryDTO questionCategoryDTO) {
        if (questionCategoryDTO != null) {
            QuestionCategory questionCategory = new QuestionCategory();

            questionCategory.setCreatedAt(questionCategoryDTO.getCreatedAt());
            questionCategory.setUpdatedAt(questionCategoryDTO.getUpdatedAt());
            questionCategory.setId(questionCategoryDTO.getId());
            questionCategory.setCategoryName(questionCategoryDTO.getCategoryName());
            questionCategory.setQuestions(questionMapper.dtosToEntities(questionCategoryDTO.getQuestionDTOS()));

            return questionCategory;
        }

        return null;
    }

    @Override
    public List<QuestionCategoryDTO> entitiesToDtos(List<QuestionCategory> questionCategories) {
        if (questionCategories != null) {
            List<QuestionCategoryDTO> questionCategoryDTOS = new ArrayList<>();

            for (QuestionCategory questionCategory : questionCategories) {
                questionCategoryDTOS.add(entityToDto(questionCategory));
            }

            return questionCategoryDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<QuestionCategory> dtosToEntities(List<QuestionCategoryDTO> questionCategoryDTOS) {
        if (questionCategoryDTOS != null) {
            List<QuestionCategory> questionCategories = new ArrayList<>();

            for (QuestionCategoryDTO questionCategoryDTO : questionCategoryDTOS) {
                questionCategories.add(dtoToEntity(questionCategoryDTO));
            }

            return questionCategories;
        }

        return Collections.emptyList();
    }
}
