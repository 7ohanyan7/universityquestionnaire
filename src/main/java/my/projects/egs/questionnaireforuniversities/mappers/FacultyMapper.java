package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;
import my.projects.egs.questionnaireforuniversities.entities.Faculty;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserResponseMapper;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class FacultyMapper implements Mapper<Faculty, FacultyDTO> {

    private UserResponseMapper userResponseMapper;
    private UniversityRepository universityRepository;

    public FacultyMapper(UserResponseMapper userResponseMapper, UniversityRepository universityRepository) {
        this.userResponseMapper = userResponseMapper;
        this.universityRepository = universityRepository;
    }


    @Override
    public FacultyDTO entityToDto(Faculty faculty) {
        if (faculty != null) {
            FacultyDTO facultyDTO = new FacultyDTO();

            facultyDTO.setCreatedAt(faculty.getCreatedAt());
            facultyDTO.setUpdatedAt(faculty.getUpdatedAt());
            facultyDTO.setId(faculty.getId());
            facultyDTO.setFacultyName(faculty.getFacultyName());
            facultyDTO.setUniversityName(faculty.getUniversity().getUniversityName());
            facultyDTO.setUserResponseDTOS(userResponseMapper.entitiesToDtos(faculty.getUsers()));

            return facultyDTO;
        }

        return null;
    }

    @Override
    public Faculty dtoToEntity(FacultyDTO facultyDTO) {
        if (facultyDTO != null) {
            Faculty faculty = new Faculty();

            faculty.setCreatedAt(facultyDTO.getCreatedAt());
            faculty.setUpdatedAt(facultyDTO.getUpdatedAt());
            faculty.setId(facultyDTO.getId());
            faculty.setFacultyName(facultyDTO.getFacultyName());
            faculty.setUniversity(universityRepository.findByUniversityName(facultyDTO.getUniversityName()).get());
            faculty.setUsers(userResponseMapper.dtosToEntities(facultyDTO.getUserResponseDTOS()));

            return faculty;
        }

        return null;
    }

    @Override
    public List<FacultyDTO> entitiesToDtos(List<Faculty> faculties) {
        if (faculties != null) {
            List<FacultyDTO> facultyDTOS = new ArrayList<>();

            for (Faculty faculty : faculties) {
                facultyDTOS.add(entityToDto(faculty));
            }

            return facultyDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Faculty> dtosToEntities(List<FacultyDTO> facultyDTOS) {
        if (facultyDTOS != null) {
            List<Faculty> faculties = new ArrayList<>();

            for (FacultyDTO facultyDTO : facultyDTOS) {
                faculties.add(dtoToEntity(facultyDTO));
            }

            return faculties;
        }

        return Collections.emptyList();
    }
}
