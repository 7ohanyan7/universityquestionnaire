package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionDTO;
import my.projects.egs.questionnaireforuniversities.entities.Question;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserInfoMapper;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionCategoryRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class QuestionMapper implements Mapper<Question, QuestionDTO> {

    private AnswerMapper answerMapper;
    private QuestionCommentMapper questionCommentMapper;
    private QuestionCategoryRepository questionCategoryRepository;
    private UserInfoMapper userInfoMapper;
    private UserRepository userRepository;

    public QuestionMapper(AnswerMapper answerMapper, QuestionCommentMapper questionCommentMapper,
                          QuestionCategoryRepository questionCategoryRepository, UserInfoMapper userInfoMapper, UserRepository userRepository) {
        this.answerMapper = answerMapper;
        this.questionCommentMapper = questionCommentMapper;
        this.questionCategoryRepository = questionCategoryRepository;
        this.userInfoMapper = userInfoMapper;
        this.userRepository = userRepository;
    }

    @Override
    public QuestionDTO entityToDto(Question question) {
        if (question != null) {
            QuestionDTO questionDTO = new QuestionDTO();

            questionDTO.setCreatedAt(question.getCreatedAt());
            questionDTO.setUpdatedAt(question.getUpdatedAt());
            questionDTO.setId(question.getId());
            questionDTO.setUserInfoDTO(userInfoMapper.entityToDto(question.getUser()));
            questionDTO.setQuestionCategoryName(question.getQuestionCategory().getCategoryName());
            questionDTO.setQuestionTitle(question.getQuestionTitle());
            questionDTO.setQuestionText(question.getQuestionText());
            questionDTO.setAnswerDTOS(answerMapper.entitiesToDtos(question.getAnswers()));
            questionDTO.setCommentDTOS(questionCommentMapper.entitiesToDtos(question.getComments()));

            return questionDTO;
        }

        return null;
    }

    @Override
    public Question dtoToEntity(QuestionDTO questionDTO) {
        if (questionDTO != null) {
            Question question = new Question();

            question.setCreatedAt(questionDTO.getCreatedAt());
            question.setUpdatedAt(questionDTO.getUpdatedAt());
            question.setId(questionDTO.getId());
            question.setUser(userRepository.findById(questionDTO.getUserInfoDTO().getId()).get());
            question.setQuestionCategory(questionCategoryRepository.findByCategoryName(questionDTO.getQuestionCategoryName()).get());
            question.setQuestionTitle(questionDTO.getQuestionTitle());
            question.setQuestionText(questionDTO.getQuestionText());
            question.setAnswers(answerMapper.dtosToEntities(questionDTO.getAnswerDTOS()));
            question.setComments(questionCommentMapper.dtosToEntities(questionDTO.getCommentDTOS()));

            return question;
        }

        return null;
    }

    @Override
    public List<QuestionDTO> entitiesToDtos(List<Question> questions) {
        if (questions != null) {
            List<QuestionDTO> questionDTOS = new ArrayList<>();

            for (Question question : questions) {
                questionDTOS.add(entityToDto(question));
            }

            return questionDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Question> dtosToEntities(List<QuestionDTO> questionDTOS) {

        if (questionDTOS != null) {
            List<Question> questions = new ArrayList<>();

            for (QuestionDTO questionDTO : questionDTOS) {
                questions.add(dtoToEntity(questionDTO));
            }

            return questions;
        }

        return Collections.emptyList();
    }
}
