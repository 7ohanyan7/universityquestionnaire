package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerDTO;
import my.projects.egs.questionnaireforuniversities.entities.Answer;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserInfoMapper;
import my.projects.egs.questionnaireforuniversities.repositories.LecturerRepository;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionRepository;
import my.projects.egs.questionnaireforuniversities.repositories.StudentRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AnswerMapper implements Mapper<Answer, AnswerDTO> {

    private AnswerCommentMapper answerCommentMapper;
    private QuestionRepository questionRepository;
    private UserRepository userRepository;
    private UserInfoMapper userInfoMapper;

    public AnswerMapper(AnswerCommentMapper answerCommentMapper, QuestionRepository questionRepository,
                        UserRepository userRepository, UserInfoMapper userInfoMapper) {
        this.answerCommentMapper = answerCommentMapper;
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.userInfoMapper = userInfoMapper;
    }

    @Override
    public AnswerDTO entityToDto(Answer answer) {
        if (answer != null) {
            AnswerDTO answerDTO = new AnswerDTO();

            answerDTO.setCreatedAt(answer.getCreatedAt());
            answerDTO.setUpdatedAt(answer.getUpdatedAt());
            answerDTO.setId(answer.getId());
            answerDTO.setAnswerText(answer.getAnswerText());
            answerDTO.setQuestionID(answer.getQuestion().getId());
            answerDTO.setUserInfoDTO(userInfoMapper.entityToDto(answer.getUser()));
            answerDTO.setCommentDTOS(answerCommentMapper.entitiesToDtos(answer.getComments()));

            return answerDTO;
        }

        return null;
    }

    @Override
    public Answer dtoToEntity(AnswerDTO answerDTO) {
        if (answerDTO != null) {
            Answer answer = new Answer();

            answer.setCreatedAt(answerDTO.getCreatedAt());
            answer.setUpdatedAt(answerDTO.getUpdatedAt());
            answer.setId(answerDTO.getId());
            answer.setAnswerText(answerDTO.getAnswerText());
            answer.setComments(answerCommentMapper.dtosToEntities(answerDTO.getCommentDTOS()));
            answer.setQuestion(questionRepository.findById(answerDTO.getId()).get());
            answer.setUser(userRepository.findById(answerDTO.getUserInfoDTO().getId()).get());

            return answer;
        }

        return null;
    }

    @Override
    public List<AnswerDTO> entitiesToDtos(List<Answer> answers) {
        if (answers != null) {
            List<AnswerDTO> answerDTOS = new ArrayList<>();

            for (Answer answer : answers) {
                answerDTOS.add(entityToDto(answer));
            }

            return answerDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Answer> dtosToEntities(List<AnswerDTO> answerDTOS) {
        if (answerDTOS != null) {
            List<Answer> answers = new ArrayList<>();

            for (AnswerDTO answerDTO : answerDTOS) {
                answers.add(dtoToEntity(answerDTO));
            }

            return answers;
        }

        return Collections.emptyList();
    }
}
