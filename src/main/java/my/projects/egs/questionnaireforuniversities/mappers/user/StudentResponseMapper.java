package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.StudentResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.Student;
import my.projects.egs.questionnaireforuniversities.mappers.Mapper;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class StudentResponseMapper implements Mapper<Student, StudentResponseDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public StudentResponseMapper(UniversityRepository universityRepository,
                                FacultyRepository facultyRepository, DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public StudentResponseDTO entityToDto(Student student) {

        if (student != null) {
            StudentResponseDTO studentResponseDTO = new StudentResponseDTO();

            studentResponseDTO.setId(student.getId());
            studentResponseDTO.setCreatedAt(student.getCreatedAt());
            studentResponseDTO.setUpdatedAt(student.getUpdatedAt());
            studentResponseDTO.setFirstName(student.getFirstName());
            studentResponseDTO.setLastName(student.getLastName());
            studentResponseDTO.setLogin(student.getLogin());
            studentResponseDTO.setBirthDate(student.getBirthDate());
            studentResponseDTO.setCountry(student.getCountry());
            studentResponseDTO.setCity(student.getCity());
            studentResponseDTO.setRegion(student.getRegion());
            studentResponseDTO.setStreet(student.getStreet());
            studentResponseDTO.setEmail(student.getEmail());
            studentResponseDTO.setPhone(student.getPhone());
            studentResponseDTO.setUniversityName(student.getUniversity().getUniversityName());
            studentResponseDTO.setFacultyName(student.getFaculty().getFacultyName());
            studentResponseDTO.setDegreeName(student.getDegree().getDegreeName());
            studentResponseDTO.setCourse(student.getCourse());
            studentResponseDTO.setRoleName(student.getRole().getRoleName());
            studentResponseDTO.setUserType(student.getUserType());

            return studentResponseDTO;
        }

        return null;
    }

    @Override
    public Student dtoToEntity(StudentResponseDTO studentResponseDTO) {

        if (studentResponseDTO != null) {
            Student student = new Student();

            student.setId(studentResponseDTO.getId());
            student.setCreatedAt(studentResponseDTO.getCreatedAt());
            student.setUpdatedAt(studentResponseDTO.getUpdatedAt());
            student.setFirstName(studentResponseDTO.getFirstName());
            student.setLastName(studentResponseDTO.getLastName());
            student.setLogin(studentResponseDTO.getLogin());
            student.setBirthDate(studentResponseDTO.getBirthDate());
            student.setCountry(studentResponseDTO.getCountry());
            student.setCity(studentResponseDTO.getCity());
            student.setRegion(studentResponseDTO.getRegion());
            student.setStreet(studentResponseDTO.getStreet());
            student.setEmail(studentResponseDTO.getEmail());
            student.setPhone(studentResponseDTO.getPhone());
            student.setCourse(studentResponseDTO.getCourse());
            student.setUserType(studentResponseDTO.getUserType());
            student.setUniversity(universityRepository.findByUniversityName(studentResponseDTO.getUniversityName()).get());
            student.setFaculty(facultyRepository.findByFacultyName(studentResponseDTO.getFacultyName()).get());
            student.setDegree(degreeRepository.findByDegreeName(studentResponseDTO.getDegreeName()).get());
            student.setRole(roleRepository.findByRoleName(studentResponseDTO.getRoleName()).get());

            return student;
        }

        return null;
    }

    @Override
    public List<StudentResponseDTO> entitiesToDtos(List<Student> students) {
        if (students != null) {
            List<StudentResponseDTO> studentResponseDTOS = new ArrayList<>();

            for (Student student : students) {
                studentResponseDTOS.add(entityToDto(student));
            }

            return studentResponseDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<Student> dtosToEntities(List<StudentResponseDTO> studentResponseDTOS) {
        if (studentResponseDTOS != null) {
            List<Student> students = new ArrayList<>();

            for (StudentResponseDTO studentResponseDTO : studentResponseDTOS) {
                students.add(dtoToEntity(studentResponseDTO));
            }

            return students;
        }

        return Collections.emptyList();
    }
}
