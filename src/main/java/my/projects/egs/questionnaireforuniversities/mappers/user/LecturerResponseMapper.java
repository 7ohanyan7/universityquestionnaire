package my.projects.egs.questionnaireforuniversities.mappers.user;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.LecturerResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.Lecturer;
import my.projects.egs.questionnaireforuniversities.mappers.Mapper;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class LecturerResponseMapper implements Mapper<Lecturer, LecturerResponseDTO> {

    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    @Autowired
    public LecturerResponseMapper(UniversityRepository universityRepository,
                              FacultyRepository facultyRepository, DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }
    
    @Override
    public LecturerResponseDTO entityToDto(Lecturer lecturer) {
        if (lecturer != null) {
            LecturerResponseDTO lecturerResponseDTO = new LecturerResponseDTO();

            lecturerResponseDTO.setId(lecturer.getId());
            lecturerResponseDTO.setCreatedAt(lecturer.getCreatedAt());
            lecturerResponseDTO.setUpdatedAt(lecturer.getUpdatedAt());
            lecturerResponseDTO.setFirstName(lecturer.getFirstName());
            lecturerResponseDTO.setLastName(lecturer.getLastName());
            lecturerResponseDTO.setLogin(lecturer.getLogin());
            lecturerResponseDTO.setBirthDate(lecturer.getBirthDate());
            lecturerResponseDTO.setCountry(lecturer.getCountry());
            lecturerResponseDTO.setCity(lecturer.getCity());
            lecturerResponseDTO.setRegion(lecturer.getRegion());
            lecturerResponseDTO.setStreet(lecturer.getStreet());
            lecturerResponseDTO.setEmail(lecturer.getEmail());
            lecturerResponseDTO.setPhone(lecturer.getPhone());
            lecturerResponseDTO.setUserType(lecturer.getUserType());
            lecturerResponseDTO.setUniversityName(lecturer.getUniversity().getUniversityName());
            lecturerResponseDTO.setFacultyName(lecturer.getFaculty().getFacultyName());
            lecturerResponseDTO.setDegreeName(lecturer.getDegree().getDegreeName());
            lecturerResponseDTO.setRoleName(lecturer.getRole().getRoleName());

            return lecturerResponseDTO;
        }

        return null;
    }

    @Override
    public Lecturer dtoToEntity(LecturerResponseDTO lecturerResponseDTO) {

        if (lecturerResponseDTO != null) {
            Lecturer lecturer = new Lecturer();

            lecturer.setId(lecturerResponseDTO.getId());
            lecturer.setCreatedAt(lecturerResponseDTO.getCreatedAt());
            lecturer.setUpdatedAt(lecturerResponseDTO.getUpdatedAt());
            lecturer.setFirstName(lecturerResponseDTO.getFirstName());
            lecturer.setLastName(lecturerResponseDTO.getLastName());
            lecturer.setLogin(lecturerResponseDTO.getLogin());
            lecturer.setBirthDate(lecturerResponseDTO.getBirthDate());
            lecturer.setCountry(lecturerResponseDTO.getCountry());
            lecturer.setCity(lecturerResponseDTO.getCity());
            lecturer.setRegion(lecturerResponseDTO.getRegion());
            lecturer.setStreet(lecturerResponseDTO.getStreet());
            lecturer.setEmail(lecturerResponseDTO.getEmail());
            lecturer.setPhone(lecturerResponseDTO.getPhone());
            lecturer.setUserType(lecturerResponseDTO.getUserType());
            lecturer.setUniversity(universityRepository.findByUniversityName(lecturerResponseDTO.getUniversityName()).get());
            lecturer.setFaculty(facultyRepository.findByFacultyName(lecturerResponseDTO.getFacultyName()).get());
            lecturer.setDegree(degreeRepository.findByDegreeName(lecturerResponseDTO.getDegreeName()).get());
            lecturer.setRole(roleRepository.findByRoleName(lecturerResponseDTO.getRoleName()).get());

            return lecturer;
        }

        return null;
    }

    @Override
    public List<LecturerResponseDTO> entitiesToDtos(List<Lecturer> lecturers) {
        if (lecturers != null) {
        List<LecturerResponseDTO> lecturerResponseDTOS = new ArrayList<>();

        for (Lecturer lecturer : lecturers) {
            lecturerResponseDTOS.add(entityToDto(lecturer));
        }

        return lecturerResponseDTOS;
    }

        return Collections.emptyList();
}

    @Override
    public List<Lecturer> dtosToEntities(List<LecturerResponseDTO> lecturerResponseDTOS) {
        if (lecturerResponseDTOS != null) {
            List<Lecturer> lecturers = new ArrayList<>();

            for (LecturerResponseDTO lecturerResponseDTO : lecturerResponseDTOS) {
                lecturers.add(dtoToEntity(lecturerResponseDTO));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }

}
