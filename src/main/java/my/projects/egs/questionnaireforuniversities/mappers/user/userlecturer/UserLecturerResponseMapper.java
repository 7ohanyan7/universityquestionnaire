package my.projects.egs.questionnaireforuniversities.mappers.user.userlecturer;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.LecturerResponseDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.Lecturer;
import my.projects.egs.questionnaireforuniversities.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UserLecturerResponseMapper {

    public Lecturer userToLecturer(User user) {

        if (user != null) {
            Lecturer lecturer = new Lecturer();

            lecturer.setId(user.getId());
            lecturer.setCreatedAt(user.getCreatedAt());
            lecturer.setUpdatedAt(user.getUpdatedAt());
            lecturer.setFirstName(user.getFirstName());
            lecturer.setLastName(user.getLastName());
            lecturer.setLogin(user.getLogin());
            lecturer.setPasswordHash(user.getPasswordHash());
            lecturer.setPasswordSalt(user.getPasswordSalt());
            lecturer.setBirthDate(user.getBirthDate());
            lecturer.setCountry(user.getCountry());
            lecturer.setCity(user.getCity());
            lecturer.setRegion(user.getRegion());
            lecturer.setStreet(user.getStreet());
            lecturer.setEmail(user.getEmail());
            lecturer.setPhone(user.getPhone());
            lecturer.setUniversity(user.getUniversity());
            lecturer.setFaculty(user.getFaculty());
            lecturer.setDegree(user.getDegree());
            lecturer.setRole(user.getRole());
            lecturer.setQuestions(user.getQuestions());
            lecturer.setAnswers(user.getAnswers());
            lecturer.setQuestionComments(user.getQuestionComments());
            lecturer.setAnswerComments(user.getAnswerComments());
            lecturer.setUserType(user.getUserType());

            return lecturer;
        }

        return null;
    }

    public LecturerResponseDTO userResponseDtoToLecturerResponseDto(UserResponseDTO userResponseDTO) {
        if (userResponseDTO != null) {
            LecturerResponseDTO lecturerResponseDTO = new LecturerResponseDTO();

            lecturerResponseDTO.setId(userResponseDTO.getId());
            lecturerResponseDTO.setCreatedAt(userResponseDTO.getCreatedAt());
            lecturerResponseDTO.setUpdatedAt(userResponseDTO.getUpdatedAt());
            lecturerResponseDTO.setFirstName(userResponseDTO.getFirstName());
            lecturerResponseDTO.setLastName(userResponseDTO.getLastName());
            lecturerResponseDTO.setLogin(userResponseDTO.getLogin());
            lecturerResponseDTO.setBirthDate(userResponseDTO.getBirthDate());
            lecturerResponseDTO.setCountry(userResponseDTO.getCountry());
            lecturerResponseDTO.setCity(userResponseDTO.getCity());
            lecturerResponseDTO.setRegion(userResponseDTO.getRegion());
            lecturerResponseDTO.setStreet(userResponseDTO.getStreet());
            lecturerResponseDTO.setEmail(userResponseDTO.getEmail());
            lecturerResponseDTO.setPhone(userResponseDTO.getPhone());
            lecturerResponseDTO.setUniversityName(userResponseDTO.getUniversityName());
            lecturerResponseDTO.setFacultyName(userResponseDTO.getFacultyName());
            lecturerResponseDTO.setDegreeName(userResponseDTO.getDegreeName());
            lecturerResponseDTO.setRoleName(userResponseDTO.getRoleName());
            lecturerResponseDTO.setUserType(userResponseDTO.getUserType());

            return lecturerResponseDTO;
        }

        return null;
    }

    public List<Lecturer> usersToLecturers(List<User> users) {
        if (users != null) {
            List<Lecturer> lecturers = new ArrayList<>();

            for (User user : users) {
                lecturers.add(userToLecturer(user));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }

    public List<LecturerResponseDTO> userResponseDtosToLecturerResponseDtos(List<UserResponseDTO> userResponseDTOS) {
        if (userResponseDTOS != null) {
            List<LecturerResponseDTO> lecturerResponseDTOS = new ArrayList<>();

            for (UserResponseDTO userResponseDTO : userResponseDTOS) {
                lecturerResponseDTOS.add(userResponseDtoToLecturerResponseDto(userResponseDTO));
            }

            return lecturerResponseDTOS;
        }

        return Collections.emptyList();
    }
}
