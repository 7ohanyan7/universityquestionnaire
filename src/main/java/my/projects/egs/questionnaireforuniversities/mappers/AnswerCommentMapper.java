package my.projects.egs.questionnaireforuniversities.mappers;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerCommentDTO;
import my.projects.egs.questionnaireforuniversities.entities.AnswerComment;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserInfoMapper;
import my.projects.egs.questionnaireforuniversities.repositories.AnswerRepository;
import my.projects.egs.questionnaireforuniversities.repositories.LecturerRepository;
import my.projects.egs.questionnaireforuniversities.repositories.StudentRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.utils.UserType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AnswerCommentMapper implements Mapper<AnswerComment, AnswerCommentDTO> {

    private AnswerRepository answerRepository;
    private UserRepository userRepository;
    private UserInfoMapper userInfoMapper;

    public AnswerCommentMapper(AnswerRepository answerRepository, UserRepository userRepository,
                               UserInfoMapper userInfoMapper) {
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
        this.userInfoMapper = userInfoMapper;
    }

    @Override
    public AnswerCommentDTO entityToDto(AnswerComment answerComment) {

        if (answerComment != null) {
            AnswerCommentDTO answerCommentDTO = new AnswerCommentDTO();

            answerCommentDTO.setCreatedAt(answerComment.getCreatedAt());
            answerCommentDTO.setUpdatedAt(answerComment.getUpdatedAt());
            answerCommentDTO.setId(answerComment.getId());
            answerCommentDTO.setAnswerID(answerComment.getAnswer().getId());
            answerCommentDTO.setCommentText(answerComment.getCommentText());
            answerCommentDTO.setUserInfoDTO(userInfoMapper.entityToDto(answerComment.getUser()));

            return answerCommentDTO;
        }

        return null;
    }

    @Override
    public AnswerComment dtoToEntity(AnswerCommentDTO answerCommentDTO) {
        if (answerCommentDTO != null) {
            AnswerComment answerComment = new AnswerComment();

            answerComment.setCreatedAt(answerCommentDTO.getCreatedAt());
            answerComment.setUpdatedAt(answerCommentDTO.getUpdatedAt());
            answerComment.setId(answerCommentDTO.getId());
            answerComment.setAnswer(answerRepository.findById(answerCommentDTO.getId()).get());
            answerComment.setCommentText(answerCommentDTO.getCommentText());
            answerComment.setUser(userRepository.findById(answerCommentDTO.getUserInfoDTO().getId()).get());


            return answerComment;
        }

        return null;
    }

    @Override
    public List<AnswerCommentDTO> entitiesToDtos(List<AnswerComment> answerComments) {
        if (answerComments != null) {
            List<AnswerCommentDTO> answerCommentDTOS = new ArrayList<>();

            for (AnswerComment answerComment : answerComments) {
                answerCommentDTOS.add(entityToDto(answerComment));
            }

            return answerCommentDTOS;
        }

        return Collections.emptyList();
    }

    @Override
    public List<AnswerComment> dtosToEntities(List<AnswerCommentDTO> answerCommentDTOS) {
        if (answerCommentDTOS != null) {
            List<AnswerComment> answerComments = new ArrayList<>();

            for (AnswerCommentDTO answerCommentDTO : answerCommentDTOS) {
                answerComments.add(dtoToEntity(answerCommentDTO));
            }

            return answerComments;
        }

        return Collections.emptyList();
    }
}
