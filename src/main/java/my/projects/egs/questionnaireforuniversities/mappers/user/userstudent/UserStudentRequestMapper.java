package my.projects.egs.questionnaireforuniversities.mappers.user.userstudent;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.UserRequestDTO;
import my.projects.egs.questionnaireforuniversities.entities.Student;
import my.projects.egs.questionnaireforuniversities.entities.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UserStudentRequestMapper {
    public Student userToStudent(User user) {

        if (user != null) {
            Student student = new Student();

            student.setId(user.getId());
            student.setCreatedAt(user.getCreatedAt());
            student.setUpdatedAt(user.getUpdatedAt());
            student.setFirstName(user.getFirstName());
            student.setLastName(user.getLastName());
            student.setLogin(user.getLogin());
            student.setPasswordHash(user.getPasswordHash());
            student.setPasswordSalt(user.getPasswordSalt());
            student.setBirthDate(user.getBirthDate());
            student.setCountry(user.getCountry());
            student.setCity(user.getCity());
            student.setRegion(user.getRegion());
            student.setStreet(user.getStreet());
            student.setEmail(user.getEmail());
            student.setPhone(user.getPhone());
            student.setUniversity(user.getUniversity());
            student.setFaculty(user.getFaculty());
            student.setDegree(user.getDegree());
            student.setRole(user.getRole());
            student.setQuestions(user.getQuestions());
            student.setAnswers(user.getAnswers());
            student.setQuestionComments(user.getQuestionComments());
            student.setAnswerComments(user.getAnswerComments());
            student.setUserType(user.getUserType());

            return student;
        }

        return null;
    }

    public StudentRequestDTO userRequestDtoToStudentRequestDto(UserRequestDTO userRequestDTO) {
        if (userRequestDTO != null) {
            StudentRequestDTO studentRequestDTO = new StudentRequestDTO();

            studentRequestDTO.setId(userRequestDTO.getId());
            studentRequestDTO.setCreatedAt(userRequestDTO.getCreatedAt());
            studentRequestDTO.setUpdatedAt(userRequestDTO.getUpdatedAt());
            studentRequestDTO.setFirstName(userRequestDTO.getFirstName());
            studentRequestDTO.setLastName(userRequestDTO.getLastName());
            studentRequestDTO.setLogin(userRequestDTO.getLogin());
            studentRequestDTO.setPasswordHash(userRequestDTO.getPasswordHash());
            studentRequestDTO.setPasswordSalt(userRequestDTO.getPasswordSalt());
            studentRequestDTO.setBirthDate(userRequestDTO.getBirthDate());
            studentRequestDTO.setCountry(userRequestDTO.getCountry());
            studentRequestDTO.setCity(userRequestDTO.getCity());
            studentRequestDTO.setRegion(userRequestDTO.getRegion());
            studentRequestDTO.setStreet(userRequestDTO.getStreet());
            studentRequestDTO.setEmail(userRequestDTO.getEmail());
            studentRequestDTO.setPhone(userRequestDTO.getPhone());
            studentRequestDTO.setUniversityName(userRequestDTO.getUniversityName());
            studentRequestDTO.setFacultyName(userRequestDTO.getFacultyName());
            studentRequestDTO.setDegreeName(userRequestDTO.getDegreeName());
            studentRequestDTO.setRoleName(userRequestDTO.getRoleName());
            studentRequestDTO.setUserType(userRequestDTO.getUserType());

            return studentRequestDTO;
        }

        return null;
    }

    public List<Student> usersToStudents(List<User> users) {
        if (users != null) {
            List<Student> lecturers = new ArrayList<>();

            for (User user : users) {
                lecturers.add(userToStudent(user));
            }

            return lecturers;
        }

        return Collections.emptyList();
    }

    public List<StudentRequestDTO> userRequestDtosToStudentRequestDtos(List<UserRequestDTO> userRequestDTOS) {
        if (userRequestDTOS != null) {
            List<StudentRequestDTO> lecturerRequestDTOS = new ArrayList<>();

            for (UserRequestDTO userRequestDTO : userRequestDTOS) {
                lecturerRequestDTOS.add(userRequestDtoToStudentRequestDto(userRequestDTO));
            }

            return lecturerRequestDTOS;
        }

        return Collections.emptyList();
    }
}
