package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import java.util.List;

public class DegreeDTO extends CreatedUpdated {
    private short id;
    private String degreeName;
    private List<UserResponseDTO> userResponseDTOS;

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public List<UserResponseDTO> getUserResponseDTOS() {
        return userResponseDTOS;
    }

    public void setUserResponseDTOS(List<UserResponseDTO> userResponseDTOS) {
        this.userResponseDTOS = userResponseDTOS;
    }
}
