package my.projects.egs.questionnaireforuniversities.dtos.user.response;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;

public class StudentResponseDTO extends UserResponseDTO {
    private byte course;

    public byte getCourse() {
        return course;
    }

    public void setCourse(byte course) {
        this.course = course;
    }
}
