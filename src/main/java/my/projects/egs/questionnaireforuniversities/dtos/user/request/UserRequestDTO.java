package my.projects.egs.questionnaireforuniversities.dtos.user.request;

import my.projects.egs.questionnaireforuniversities.dtos.*;
import my.projects.egs.questionnaireforuniversities.dtos.user.UserDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import java.sql.Date;
import java.util.List;

public class UserRequestDTO extends UserDTO {
    private String passwordSalt;
    private String passwordHash;

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
