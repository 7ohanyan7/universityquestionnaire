package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;
import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;

import java.util.List;

@CheckDBForSave(
        message = "{chechdbforsave.university}",
        fieldName = "universityName"
)
public class UniversityDTO extends CreatedUpdated {
    private int id;
    private String universityName;
    private List<UserResponseDTO> userResponseDTOS;
    private List<FacultyDTO> facultyDTOS;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public List<UserResponseDTO> getUserResponseDTOS() {
        return userResponseDTOS;
    }

    public void setUserResponseDTOS(List<UserResponseDTO> userResponseDTOS) {
        this.userResponseDTOS = userResponseDTOS;
    }

    public List<FacultyDTO> getFacultyDTOS() {
        return facultyDTOS;
    }

    public void setFacultyDTOS(List<FacultyDTO> facultyDTOS) {
        this.facultyDTOS = facultyDTOS;
    }
}
