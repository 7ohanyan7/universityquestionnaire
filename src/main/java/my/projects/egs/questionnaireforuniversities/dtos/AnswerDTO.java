package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.UserInfoDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import javax.validation.constraints.Size;
import java.util.List;

public class AnswerDTO extends CreatedUpdated {
    private long id;
    @Size(min = 20, message = "{answerText}")
    private String answerText;
    private long questionID;
    private UserInfoDTO userInfoDTO;
    private List<AnswerCommentDTO> commentDTOS;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public List<AnswerCommentDTO> getCommentDTOS() {
        return commentDTOS;
    }

    public void setCommentDTOS(List<AnswerCommentDTO> commentDTOS) {
        this.commentDTOS = commentDTOS;
    }

    public long getQuestionID() {
        return questionID;
    }

    public void setQuestionID(long questionID) {
        this.questionID = questionID;
    }

    public UserInfoDTO getUserInfoDTO() {
        return userInfoDTO;
    }

    public void setUserInfoDTO(UserInfoDTO userInfoDTO) {
        this.userInfoDTO = userInfoDTO;
    }
}
