package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.UserInfoDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import javax.validation.constraints.Size;

public class QuestionCommentDTO extends CreatedUpdated {
    private long id;
    @Size(min = 20, message = "{commentText}")
    private String commentText;
    private long questionID;
    private UserInfoDTO userInfoDTO;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public long getQuestionID() {
        return questionID;
    }

    public void setQuestionID(long questionID) {
        this.questionID = questionID;
    }

    public UserInfoDTO getUserInfoDTO() {
        return userInfoDTO;
    }

    public void setUserInfoDTO(UserInfoDTO userInfoDTO) {
        this.userInfoDTO = userInfoDTO;
    }
}
