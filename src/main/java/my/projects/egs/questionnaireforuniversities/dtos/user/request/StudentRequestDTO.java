package my.projects.egs.questionnaireforuniversities.dtos.user.request;

import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@CheckDBForSave(
        message = "There is already a user with this login",
        fieldName = "login"
)
public class StudentRequestDTO extends UserRequestDTO {
    @Min(value = 1, message = "{course.min}")
    @Max(value = 6, message = "{course.max}")
    private byte course;

    public byte getCourse() {
        return course;
    }

    public void setCourse(byte course) {
        this.course = course;
    }
}
