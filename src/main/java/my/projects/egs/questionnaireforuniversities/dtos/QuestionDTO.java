package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.UserInfoDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import javax.validation.constraints.Size;
import java.util.List;

public class QuestionDTO extends CreatedUpdated {
    private long id;
    @Size(min = 5, message = "{questionTitle}")
    private String questionTitle;
    @Size(min = 20, message = "{questionText}")
    private String questionText;
    private String questionCategoryName;
    private UserInfoDTO userInfoDTO;
    private List<AnswerDTO> answerDTOS;
    private List<QuestionCommentDTO> commentDTOS;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionCategoryName() {
        return questionCategoryName;
    }

    public void setQuestionCategoryName(String questionCategoryName) {
        this.questionCategoryName = questionCategoryName;
    }

    public List<AnswerDTO> getAnswerDTOS() {
        return answerDTOS;
    }

    public void setAnswerDTOS(List<AnswerDTO> answerDTOS) {
        this.answerDTOS = answerDTOS;
    }

    public List<QuestionCommentDTO> getCommentDTOS() {
        return commentDTOS;
    }

    public void setCommentDTOS(List<QuestionCommentDTO> commentDTOS) {
        this.commentDTOS = commentDTOS;
    }

    public UserInfoDTO getUserInfoDTO() {
        return userInfoDTO;
    }

    public void setUserInfoDTO(UserInfoDTO userInfoDTO) {
        this.userInfoDTO = userInfoDTO;
    }
}
