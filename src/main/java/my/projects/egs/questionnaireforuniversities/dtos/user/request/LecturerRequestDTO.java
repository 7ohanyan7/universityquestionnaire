package my.projects.egs.questionnaireforuniversities.dtos.user.request;

import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;

@CheckDBForSave(
        message = "There is already a user with this login",
        fieldName = "login"
)
public class LecturerRequestDTO extends UserRequestDTO {
}
