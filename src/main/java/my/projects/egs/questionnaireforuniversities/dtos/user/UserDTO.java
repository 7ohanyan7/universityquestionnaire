package my.projects.egs.questionnaireforuniversities.dtos.user;

import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateBirthDate;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateDegree;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateFaculty;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateUniversity;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.sql.Date;

@ValidateFaculty(
        facultyFieldName = "facultyName",
        universityFieldName = "universityName",
        message = "{facultyName}")
public class UserDTO extends CreatedUpdated {
    private long id;
    @Size(min = 3, message = "{firstName}")
    private String firstName;
    @Size(min = 3, message = "{lastName}")
    private String lastName;
    @Size(min = 3, message = "{login}")
    private String login;
    @ValidateBirthDate(message = "{birthDate}")
    private Date birthDate;
    private String country;
    private String city;
    private String region;
    private String street;
    @Email(message = "{email}")
    private String email;
    private String phone;
    @ValidateUniversity(message = "{universityName}")
    private String universityName;
    private String facultyName;
    @ValidateDegree(message = "{degreeName}")
    private String degreeName;
    private String roleName;
    private String userType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
