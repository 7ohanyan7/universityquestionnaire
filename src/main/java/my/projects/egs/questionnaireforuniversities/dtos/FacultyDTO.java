package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;
import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;

import java.util.List;

@CheckDBForSave(
        fieldName = "facultyName",
        helperFieldName = "universityName",
        message = "{checkdbforuniversity.faculty}")
public class FacultyDTO extends CreatedUpdated {
    private int id;
    private String facultyName;
    private String universityName;
    private List<UserResponseDTO> userResponseDTOS;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public List<UserResponseDTO> getUserResponseDTOS() {
        return userResponseDTOS;
    }

    public void setUserResponseDTOS(List<UserResponseDTO> userResponseDTOS) {
        this.userResponseDTOS = userResponseDTOS;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
