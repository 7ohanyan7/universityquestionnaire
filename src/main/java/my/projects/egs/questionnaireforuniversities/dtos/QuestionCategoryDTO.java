package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;
import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;

import java.util.List;

@CheckDBForSave(
        fieldName = "categoryName",
        message = "{checkdbforuniversity.questioncategory}")
public class QuestionCategoryDTO extends CreatedUpdated {
    private int id;
    private String categoryName;
    private List<QuestionDTO> questionDTOS;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<QuestionDTO> getQuestionDTOS() {
        return questionDTOS;
    }

    public void setQuestionDTOS(List<QuestionDTO> questionDTOS) {
        this.questionDTOS = questionDTOS;
    }
}
