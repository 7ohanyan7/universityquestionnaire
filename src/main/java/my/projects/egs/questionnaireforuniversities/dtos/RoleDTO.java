package my.projects.egs.questionnaireforuniversities.dtos;

import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import java.util.List;

public class RoleDTO extends CreatedUpdated {
    private byte id;
    private String roleName;
    private List<UserResponseDTO> userResponseDTOS;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<UserResponseDTO> getUserResponseDTOS() {
        return userResponseDTOS;
    }

    public void setUserResponseDTOS(List<UserResponseDTO> userResponseDTOS) {
        this.userResponseDTOS = userResponseDTOS;
    }
}
