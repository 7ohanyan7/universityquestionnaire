package my.projects.egs.questionnaireforuniversities.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "LECTURER")
public class Lecturer extends User {
}
