package my.projects.egs.questionnaireforuniversities.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import javax.persistence.*;
import java.util.List;

@Entity
public class University extends CreatedUpdated {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true)
    private String universityName;
    @OneToMany(mappedBy = "university", fetch = FetchType.LAZY)
    private List<User> users;
    @OneToMany(mappedBy = "university", fetch = FetchType.LAZY)
    private List<Faculty> faculties;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Faculty> getFaculties() {
        return faculties;
    }

    public void setFaculties(List<Faculty> faculties) {
        this.faculties = faculties;
    }
}
