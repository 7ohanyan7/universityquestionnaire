package my.projects.egs.questionnaireforuniversities.entities;

import my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses.CreatedUpdated;

import javax.persistence.*;
import java.util.List;

@Entity
public class Role extends CreatedUpdated {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private byte id;
    @Column(unique = true)
    private String roleName;
    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private List<User> users;

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
