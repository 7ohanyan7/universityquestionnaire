package my.projects.egs.questionnaireforuniversities.entities.mappedsuperclasses;

import javax.persistence.MappedSuperclass;
import java.sql.Date;

@MappedSuperclass
public class CreatedUpdated {
    private Date createdAt;
    private Date updatedAt;

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
