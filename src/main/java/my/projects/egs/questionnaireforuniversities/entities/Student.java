package my.projects.egs.questionnaireforuniversities.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "STUDENT")
public class Student extends User {
    private byte course;

    public byte getCourse() {
        return course;
    }

    public void setCourse(byte course) {
        this.course = course;
    }
}
