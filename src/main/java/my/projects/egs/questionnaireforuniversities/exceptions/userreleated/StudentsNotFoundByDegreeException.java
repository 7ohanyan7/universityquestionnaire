package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class StudentsNotFoundByDegreeException extends RuntimeException {

    private final String degreeName;

    public StudentsNotFoundByDegreeException(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeName() {
        return degreeName;
    }
}
