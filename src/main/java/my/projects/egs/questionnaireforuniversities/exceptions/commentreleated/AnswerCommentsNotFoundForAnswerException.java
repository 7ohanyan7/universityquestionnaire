package my.projects.egs.questionnaireforuniversities.exceptions.commentreleated;

public class AnswerCommentsNotFoundForAnswerException extends RuntimeException {

    private final long answerCommentId;

    public AnswerCommentsNotFoundForAnswerException(long answerCommentId) {
        this.answerCommentId = answerCommentId;
    }

    public long getAnswerCommentId() {
        return answerCommentId;
    }
}
