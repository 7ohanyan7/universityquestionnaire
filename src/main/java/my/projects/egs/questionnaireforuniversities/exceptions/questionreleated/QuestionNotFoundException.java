package my.projects.egs.questionnaireforuniversities.exceptions.questionreleated;

public class QuestionNotFoundException extends RuntimeException {

    private final long questionId;

    public QuestionNotFoundException(long questionId) {
        this.questionId = questionId;
    }

    public long getQuestionId() {
        return questionId;
    }
}
