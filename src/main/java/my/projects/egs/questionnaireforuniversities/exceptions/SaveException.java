package my.projects.egs.questionnaireforuniversities.exceptions;

import org.springframework.validation.Errors;

public class SaveException extends RuntimeException {

    private final Errors errors;

    public SaveException(Errors errors) {
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
