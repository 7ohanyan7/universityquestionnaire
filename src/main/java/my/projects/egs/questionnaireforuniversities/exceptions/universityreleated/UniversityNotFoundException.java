package my.projects.egs.questionnaireforuniversities.exceptions.universityreleated;

public class UniversityNotFoundException extends RuntimeException {

    private final int universityId;

    public UniversityNotFoundException(int universityId) {
        this.universityId = universityId;
    }

    public int getUniversityId() {
        return universityId;
    }
}
