package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class StudentsNotFoundInUniversityException extends RuntimeException {

    private final String universityName;

    public StudentsNotFoundInUniversityException(String universityName) {
        this.universityName = universityName;
    }

    public String getUniversityName() {
        return universityName;
    }
}
