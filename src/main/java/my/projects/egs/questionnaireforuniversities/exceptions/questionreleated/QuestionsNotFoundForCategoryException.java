package my.projects.egs.questionnaireforuniversities.exceptions.questionreleated;

public class QuestionsNotFoundForCategoryException extends RuntimeException {

    private final String categoryName;

    public QuestionsNotFoundForCategoryException(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }
}
