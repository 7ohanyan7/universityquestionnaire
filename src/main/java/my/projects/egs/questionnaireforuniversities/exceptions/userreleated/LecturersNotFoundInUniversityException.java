package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class LecturersNotFoundInUniversityException extends RuntimeException {

    private final String universityName;

    public LecturersNotFoundInUniversityException(String universityName) {
        this.universityName = universityName;
    }

    public String getUniversityName() {
        return universityName;
    }
}
