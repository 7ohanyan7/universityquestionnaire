package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class LecturersNotFoundInFacultyException extends RuntimeException {

    private final String facultyName;

    public LecturersNotFoundInFacultyException(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getFacultyName() {
        return facultyName;
    }
}
