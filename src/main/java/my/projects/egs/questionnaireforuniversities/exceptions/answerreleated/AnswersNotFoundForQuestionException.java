package my.projects.egs.questionnaireforuniversities.exceptions.answerreleated;

public class AnswersNotFoundForQuestionException extends RuntimeException {

    private final long questionId;

    public AnswersNotFoundForQuestionException(long questionId) {
        this.questionId = questionId;
    }

    public long getQuestionId() {
        return questionId;
    }
}
