package my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated;

public class FacultiesNotFoundForUniversityException extends RuntimeException {

    private final String universityName;

    public FacultiesNotFoundForUniversityException(String universityName) {
        this.universityName = universityName;
    }

    public String getUniversityName() {
        return universityName;
    }
}
