package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class StudentsNotFoundInFacultyException extends RuntimeException {

    private final String facultyName;

    public StudentsNotFoundInFacultyException(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getFacultyName() {
        return facultyName;
    }
}
