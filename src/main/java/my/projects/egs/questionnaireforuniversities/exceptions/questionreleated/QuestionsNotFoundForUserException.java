package my.projects.egs.questionnaireforuniversities.exceptions.questionreleated;

public class QuestionsNotFoundForUserException extends RuntimeException {

    private final long userId;

    public QuestionsNotFoundForUserException(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }
}
