package my.projects.egs.questionnaireforuniversities.exceptions.facultyreleated;

public class FacultyNotFoundException extends RuntimeException {

    private final int facultyId;

    public FacultyNotFoundException(int facultyId) {
        this.facultyId = facultyId;
    }

    public int getFacultyId() {
        return facultyId;
    }
}
