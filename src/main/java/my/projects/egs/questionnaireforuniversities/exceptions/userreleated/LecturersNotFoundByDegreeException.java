package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class LecturersNotFoundByDegreeException extends RuntimeException {

    private final String degreeName;

    public LecturersNotFoundByDegreeException(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDegreeName() {
        return degreeName;
    }
}
