package my.projects.egs.questionnaireforuniversities.exceptions.answerreleated;

public class AnswersNotFoundForUserException extends RuntimeException {

    private final long userId;

    public AnswersNotFoundForUserException(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }
}
