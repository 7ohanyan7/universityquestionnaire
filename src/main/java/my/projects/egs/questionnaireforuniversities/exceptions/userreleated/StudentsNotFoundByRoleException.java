package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class StudentsNotFoundByRoleException extends RuntimeException {

    private final String roleName;

    public StudentsNotFoundByRoleException(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
