package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class LecturersNotFoundByRoleException extends RuntimeException {

    private final String roleName;

    public LecturersNotFoundByRoleException(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
