package my.projects.egs.questionnaireforuniversities.exceptions.commentreleated;

public class QuestionCommentNotFoundException extends RuntimeException {

    private final long questionCommentId;

    public QuestionCommentNotFoundException(long questionCommentId) {
        this.questionCommentId = questionCommentId;
    }

    public long getQuestionCommentId() {
        return questionCommentId;
    }
}
