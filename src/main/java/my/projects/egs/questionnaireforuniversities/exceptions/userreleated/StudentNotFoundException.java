package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class StudentNotFoundException extends RuntimeException {

    private final long studentId;

    public StudentNotFoundException(long studentId) {
        this.studentId = studentId;
    }

    public long getStudentId() {
        return studentId;
    }
}
