package my.projects.egs.questionnaireforuniversities.exceptions.commentreleated;

public class AnswerCommentNotFoundException extends RuntimeException {

    private final long answerCommentId;

    public AnswerCommentNotFoundException(long answerCommentId) {
        this.answerCommentId = answerCommentId;
    }

    public long getAnswerCommentId() {
        return answerCommentId;
    }
}
