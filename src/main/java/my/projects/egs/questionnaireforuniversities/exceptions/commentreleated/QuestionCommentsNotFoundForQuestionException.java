package my.projects.egs.questionnaireforuniversities.exceptions.commentreleated;

public class QuestionCommentsNotFoundForQuestionException extends RuntimeException {

    private final long questionId;

    public QuestionCommentsNotFoundForQuestionException(long questionId) {
        this.questionId = questionId;
    }

    public long getQuestionId() {
        return questionId;
    }
}
