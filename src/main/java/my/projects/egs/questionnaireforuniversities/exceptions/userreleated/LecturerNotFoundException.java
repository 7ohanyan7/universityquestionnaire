package my.projects.egs.questionnaireforuniversities.exceptions.userreleated;

public class LecturerNotFoundException extends RuntimeException {

    private final long lecturerId;

    public LecturerNotFoundException(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public long getLecturerId() {
        return lecturerId;
    }
}
