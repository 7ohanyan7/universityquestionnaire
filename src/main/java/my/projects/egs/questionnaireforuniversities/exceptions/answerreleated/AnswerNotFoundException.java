package my.projects.egs.questionnaireforuniversities.exceptions.answerreleated;

public class AnswerNotFoundException extends RuntimeException {

    private final long answerId;

    public AnswerNotFoundException(int answerId) {
        this.answerId = answerId;
    }

    public long getAnswerId() {
        return answerId;
    }
}
