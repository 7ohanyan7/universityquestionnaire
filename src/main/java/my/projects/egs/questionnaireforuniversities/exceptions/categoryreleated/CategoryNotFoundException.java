package my.projects.egs.questionnaireforuniversities.exceptions.categoryreleated;

public class CategoryNotFoundException extends RuntimeException {

    private final int categoryId;

    public CategoryNotFoundException(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
