package my.projects.egs.questionnaireforuniversities.repositories;

import my.projects.egs.questionnaireforuniversities.entities.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    Optional<Answer> findById(long id);

}
