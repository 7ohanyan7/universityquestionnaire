package my.projects.egs.questionnaireforuniversities.repositories;

import my.projects.egs.questionnaireforuniversities.entities.Degree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DegreeRepository extends JpaRepository<Degree, Short> {

    Optional<Degree> findByDegreeName(String degreeName);

}
