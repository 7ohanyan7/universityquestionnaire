package my.projects.egs.questionnaireforuniversities.repositories;

import my.projects.egs.questionnaireforuniversities.entities.QuestionCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface QuestionCategoryRepository extends JpaRepository<QuestionCategory, Integer> {

    Optional<QuestionCategory> findByCategoryName(String categoryName);

}
