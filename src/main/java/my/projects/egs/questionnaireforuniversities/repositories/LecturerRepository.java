package my.projects.egs.questionnaireforuniversities.repositories;

import my.projects.egs.questionnaireforuniversities.entities.Lecturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LecturerRepository extends JpaRepository<Lecturer,Long> {

    Optional<Lecturer> findByLogin(String login);

}
