package my.projects.egs.questionnaireforuniversities.repositories;

import my.projects.egs.questionnaireforuniversities.entities.AnswerComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerCommentRepository extends JpaRepository<AnswerComment, Long> {



}
