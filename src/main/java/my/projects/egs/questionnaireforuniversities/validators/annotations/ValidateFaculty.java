package my.projects.egs.questionnaireforuniversities.validators.annotations;


import my.projects.egs.questionnaireforuniversities.validators.FacultyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = FacultyValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateFaculty {

    String message() default "Invalid faculty";

    String facultyFieldName();

    String universityFieldName();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}