package my.projects.egs.questionnaireforuniversities.validators;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.UserDTO;
import my.projects.egs.questionnaireforuniversities.sevices.FacultyService;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateFaculty;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

@Component
public class FacultyValidator implements ConstraintValidator<ValidateFaculty, Object> {

    private FacultyService facultyService;
    private String facultyFieldName;
    private String universityFieldName;

    @Autowired
    public FacultyValidator(FacultyService facultyService) {
        this.facultyService = facultyService;
    }

    @Override
    public void initialize(ValidateFaculty constraintAnnotation) {
        this.facultyFieldName = constraintAnnotation.facultyFieldName();
        this.universityFieldName = constraintAnnotation.universityFieldName();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        List<FacultyDTO> faculties = null;
        List<String> facultyNames = new ArrayList<>();
        Object facultyName = new BeanWrapperImpl(o)
                .getPropertyValue(facultyFieldName);
        Object universityName = new BeanWrapperImpl(o)
                .getPropertyValue(universityFieldName);

        if (universityName != null && facultyName != null) {
            faculties = facultyService.findByUniversityName((String) universityName);
            for (FacultyDTO facultyDTO : faculties) {
                facultyNames.add(facultyDTO.getFacultyName());
            }
        }

        return facultyNames.contains(facultyName);
    }
}
