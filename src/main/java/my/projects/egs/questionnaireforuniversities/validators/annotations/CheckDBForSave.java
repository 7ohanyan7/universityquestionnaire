package my.projects.egs.questionnaireforuniversities.validators.annotations;

import my.projects.egs.questionnaireforuniversities.validators.CheckDBValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CheckDBValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckDBForSave {

    String message() default "DB already contain this";

    String fieldName();

    String helperFieldName() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}