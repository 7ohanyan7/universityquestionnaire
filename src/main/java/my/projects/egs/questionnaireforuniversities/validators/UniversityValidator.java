package my.projects.egs.questionnaireforuniversities.validators;

import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;
import my.projects.egs.questionnaireforuniversities.sevices.UniversityService;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateUniversity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

@Component
public class UniversityValidator implements ConstraintValidator<ValidateUniversity, String> {

    private UniversityService universityService;

    @Autowired
    public UniversityValidator(UniversityService universityService) {
        this.universityService = universityService;
    }

    @Override
    public void initialize(ValidateUniversity constraintAnnotation) {

    }

    @Override
    public boolean isValid(String universityName, ConstraintValidatorContext constraintValidatorContext) {
        List<UniversityDTO> universities = universityService.findAll();
        List<String> universityNames = new ArrayList<>();
        for (UniversityDTO universityDTO : universities) {
            universityNames.add(universityDTO.getUniversityName());
        }
        return universityNames.contains(universityName);
    }
}
