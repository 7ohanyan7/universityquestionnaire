package my.projects.egs.questionnaireforuniversities.validators.annotations;

import my.projects.egs.questionnaireforuniversities.validators.DegreeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = DegreeValidator.class)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateDegree {

    String message() default "Invalid degree";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}