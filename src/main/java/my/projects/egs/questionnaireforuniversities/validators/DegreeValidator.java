package my.projects.egs.questionnaireforuniversities.validators;

import my.projects.egs.questionnaireforuniversities.dtos.DegreeDTO;
import my.projects.egs.questionnaireforuniversities.sevices.DegreeService;
import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateDegree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

@Component
public class DegreeValidator implements ConstraintValidator<ValidateDegree, String> {

    private DegreeService degreeService;

    @Autowired
    public DegreeValidator(DegreeService degreeService) {
        this.degreeService = degreeService;
    }

    @Override
    public void initialize(ValidateDegree constraintAnnotation) {

    }

    @Override
    public boolean isValid(String degreeName, ConstraintValidatorContext constraintValidatorContext) {
        List<DegreeDTO> degrees = degreeService.findAll();
        List<String> degreeNames = new ArrayList<>();
        for (DegreeDTO degreeDTO : degrees) {
            degreeNames.add(degreeDTO.getDegreeName());
        }
        return degreeNames.contains(degreeName);
    }
}
