package my.projects.egs.questionnaireforuniversities.validators.annotations;

import my.projects.egs.questionnaireforuniversities.validators.UniversityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniversityValidator.class)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateUniversity {

    String message() default "Invalid university";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}