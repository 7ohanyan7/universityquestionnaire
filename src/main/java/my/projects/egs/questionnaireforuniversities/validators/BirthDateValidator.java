package my.projects.egs.questionnaireforuniversities.validators;

import my.projects.egs.questionnaireforuniversities.validators.annotations.ValidateBirthDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneOffset;

public class BirthDateValidator implements ConstraintValidator<ValidateBirthDate, Date> {
   public void initialize(ValidateBirthDate constraint) {
   }

   public boolean isValid(Date birthDate, ConstraintValidatorContext context) {
      return birthDate.before(Date.valueOf(LocalDate.now()));
   }
}
