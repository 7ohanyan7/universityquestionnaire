package my.projects.egs.questionnaireforuniversities.validators;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;
import my.projects.egs.questionnaireforuniversities.dtos.QuestionCategoryDTO;
import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.sevices.*;
import my.projects.egs.questionnaireforuniversities.sevices.impl.UserServiceImpl;
import my.projects.egs.questionnaireforuniversities.validators.annotations.CheckDBForSave;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

@Component
public class CheckDBValidator implements ConstraintValidator<CheckDBForSave, Object> {

   private String fieldName;
   private String helperFieldName;

   private UserServiceImpl userService;
   private FacultyService facultyService;
   private QuestionCategoryService questionCategoryService;
   private UniversityService universityService;

   @Autowired
   public CheckDBValidator(UserServiceImpl userService, FacultyService facultyService,
                           QuestionCategoryService questionCategoryService, UniversityService universityService) {
      this.userService = userService;

      this.facultyService = facultyService;
      this.questionCategoryService = questionCategoryService;
      this.universityService = universityService;
   }

   public void initialize(CheckDBForSave constraint) {
      fieldName = constraint.fieldName();
      helperFieldName = constraint.helperFieldName();
   }

   public boolean isValid(Object requestedObject, ConstraintValidatorContext context) {
      String className = requestedObject.getClass().getName();
      String info;
      if (className.equals(UniversityDTO.class.getName())) {
         info = (String) new BeanWrapperImpl(requestedObject)
                 .getPropertyValue(fieldName);
         return universityService.findByUniversityName(info) == null;
      }else if (className.equals(FacultyDTO.class.getName())) {
         String univeristyName = (String) new BeanWrapperImpl(requestedObject)
                 .getPropertyValue(helperFieldName);
         info = (String) new BeanWrapperImpl(requestedObject)
                 .getPropertyValue(fieldName);
         List<FacultyDTO> faculties = facultyService.findByUniversityName(univeristyName);
         List<String> facultyNames = new ArrayList<>();
         for (FacultyDTO facultyDTO : faculties) {
            facultyNames.add(facultyDTO.getFacultyName());
         }
         return !facultyNames.contains(info);
      }else if (className.equals(StudentRequestDTO.class.getName()) || className.equals(LecturerRequestDTO.class.getName())) {
         info = (String) new BeanWrapperImpl(requestedObject)
                 .getPropertyValue(fieldName);
         return userService.findByLogin(info) == null;
      }else if (className.equals(QuestionCategoryDTO.class.getName())) {
         info = (String) new BeanWrapperImpl(requestedObject)
                 .getPropertyValue(fieldName);
         return questionCategoryService.findByCategoryName(info) == null;
      }
      return false;
   }
}
