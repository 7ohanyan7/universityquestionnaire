package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.LecturerResponseDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.StudentResponseDTO;

public interface StudentService extends UserService<StudentRequestDTO, StudentResponseDTO> {
}
