package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;

import java.util.List;
import java.util.Optional;

public interface FacultyService {

    FacultyDTO save(FacultyDTO answerDTO);

    FacultyDTO findByID(long id);

    FacultyDTO findByFacultyName(String facultyName);

    List<FacultyDTO> findByUniversityName(String universityName);

    List<FacultyDTO> findAll();

    void deleteById(int id);

}
