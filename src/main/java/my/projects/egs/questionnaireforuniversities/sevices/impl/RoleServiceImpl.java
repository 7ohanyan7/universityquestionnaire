package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.RoleDTO;
import my.projects.egs.questionnaireforuniversities.mappers.RoleMapper;
import my.projects.egs.questionnaireforuniversities.repositories.RoleRepository;
import my.projects.egs.questionnaireforuniversities.sevices.RoleService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleMapper roleMapper;
    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleMapper roleMapper, RoleRepository roleRepository) {
        this.roleMapper = roleMapper;
        this.roleRepository = roleRepository;
    }

    @Override
    public RoleDTO save(RoleDTO roleDTO) {
        if (roleDTO.getCreatedAt() == null) {
            roleDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        roleDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return roleMapper.entityToDto(roleRepository.save(roleMapper.dtoToEntity(roleDTO)));
    }

    @Override
    public List<RoleDTO> findAll() {
        return roleMapper.entitiesToDtos(roleRepository.findAll());
    }

    @Override
    public void deleteById(byte id) {
        roleRepository.deleteById(id);
    }
}
