package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerCommentDTO;

import java.util.List;

public interface AnswerCommentService {

    AnswerCommentDTO save(AnswerCommentDTO answerCommentDTO);

    AnswerCommentDTO findByID(long id);

    List<AnswerCommentDTO> findAll();

    List<AnswerCommentDTO> findByAnswerId(long id);

    void deleteById(long id);

}
