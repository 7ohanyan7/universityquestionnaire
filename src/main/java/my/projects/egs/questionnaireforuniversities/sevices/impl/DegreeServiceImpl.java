package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.DegreeDTO;
import my.projects.egs.questionnaireforuniversities.entities.Degree;
import my.projects.egs.questionnaireforuniversities.mappers.DegreeMapper;
import my.projects.egs.questionnaireforuniversities.repositories.DegreeRepository;
import my.projects.egs.questionnaireforuniversities.sevices.DegreeService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class DegreeServiceImpl implements DegreeService {

    private DegreeMapper degreeMapper;
    private DegreeRepository degreeRepository;

    public DegreeServiceImpl(DegreeMapper degreeMapper, DegreeRepository degreeRepository) {
        this.degreeMapper = degreeMapper;
        this.degreeRepository = degreeRepository;
    }

    @Override
    public DegreeDTO save(DegreeDTO degreeDTO) {
        if (degreeDTO.getCreatedAt() == null) {
            degreeDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        degreeDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return degreeMapper.entityToDto(degreeMapper.dtoToEntity(degreeDTO));
    }

    @Override
    public DegreeDTO findByID(long id) {
        Optional<Degree> degreeOptional = degreeRepository.findById((short) id);
        if (degreeOptional.isPresent()) {
            return degreeMapper.entityToDto(degreeOptional.get());
        }
        return null;
    }

    @Override
    public DegreeDTO findByDegreeName(String degreeName) {
        Optional<Degree> degreeOptional = degreeRepository.findByDegreeName(degreeName);
        if (degreeOptional.isPresent()) {
            return degreeMapper.entityToDto(degreeOptional.get());
        }
        return null;
    }

    @Override
    public List<DegreeDTO> findAll() {
        return degreeMapper.entitiesToDtos(degreeRepository.findAll());
    }

    @Override
    public void deleteById(short id) {
        degreeRepository.deleteById(id);
    }


}
