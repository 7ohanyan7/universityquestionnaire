package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCommentDTO;
import my.projects.egs.questionnaireforuniversities.entities.Question;
import my.projects.egs.questionnaireforuniversities.entities.QuestionComment;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.mappers.QuestionCommentMapper;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionCommentRepository;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionCommentService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionCommentServiceImpl implements QuestionCommentService {

    private QuestionCommentMapper questionCommentMapper;
    private QuestionCommentRepository questionCommentRepository;
    private UserRepository userRepository;
    private QuestionRepository questionRepository;

    public QuestionCommentServiceImpl(QuestionCommentMapper questionCommentMapper, QuestionCommentRepository questionCommentRepository,
                                      UserRepository userRepository, QuestionRepository questionRepository) {
        this.questionCommentMapper = questionCommentMapper;
        this.questionCommentRepository = questionCommentRepository;
        this.userRepository = userRepository;
        this.questionRepository = questionRepository;
    }

    @Override
    public QuestionCommentDTO save(QuestionCommentDTO questionCommentDTO) {
        if (questionCommentDTO.getCreatedAt() == null) {
            questionCommentDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        questionCommentDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return questionCommentMapper.entityToDto(questionCommentRepository.save(questionCommentMapper.dtoToEntity(questionCommentDTO)));
    }

    @Override
    public QuestionCommentDTO findByID(long id) {
        Optional<QuestionComment> questionCommentOptional = questionCommentRepository.findById(id);
        if (questionCommentOptional.isPresent()) {
            return questionCommentMapper.entityToDto(questionCommentOptional.get());
        }
        return null;
    }

    @Override
    public List<QuestionCommentDTO> findByUserLogin(String userLogin) {
        Optional<User> userOptional = userRepository.findByLogin(userLogin);
        if (userOptional.isPresent()) {
            return questionCommentMapper.entitiesToDtos(userOptional.get().getQuestionComments());
        }
        return Collections.emptyList();
    }

    @Override
    public List<QuestionCommentDTO> findByQuestionId(long id) {
        Optional<Question> questionOptional = questionRepository.findById(id);
        if (questionOptional.isPresent()) {
            return questionCommentMapper.entitiesToDtos(questionOptional.get().getComments());
        }
        return Collections.emptyList();
    }

    @Override
    public List<QuestionCommentDTO> findAll() {
        return questionCommentMapper.entitiesToDtos(questionCommentRepository.findAll());
    }

    @Override
    public void deleteById(long id) {
        questionCommentRepository.deleteById(id);
    }
}
