package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.StudentRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.StudentResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.*;
import my.projects.egs.questionnaireforuniversities.mappers.user.StudentRequestMapper;
import my.projects.egs.questionnaireforuniversities.mappers.user.StudentResponseMapper;
import my.projects.egs.questionnaireforuniversities.mappers.user.userstudent.UserStudentResponseMapper;
import my.projects.egs.questionnaireforuniversities.repositories.*;
import my.projects.egs.questionnaireforuniversities.sevices.StudentService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    private UserStudentResponseMapper userStudentResponseMapper;
    private StudentResponseMapper studentResponseMapper;
    private StudentRequestMapper studentRequestMapper;
    private StudentRepository studentRepository;
    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    public StudentServiceImpl(UserStudentResponseMapper userStudentResponseMapper, StudentResponseMapper studentResponseMapper,
                              StudentRequestMapper studentRequestMapper, StudentRepository studentRepository,
                              UniversityRepository universityRepository, FacultyRepository facultyRepository,
                              DegreeRepository degreeRepository, RoleRepository roleRepository) {
        this.userStudentResponseMapper = userStudentResponseMapper;
        this.studentResponseMapper = studentResponseMapper;
        this.studentRequestMapper = studentRequestMapper;
        this.studentRepository = studentRepository;
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public StudentResponseDTO save(StudentRequestDTO studentRequestDTO) {
        if (studentRequestDTO.getCreatedAt() == null) {
            studentRequestDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        studentRequestDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return studentResponseMapper.entityToDto(studentRepository.save(studentRequestMapper.dtoToEntity(studentRequestDTO)));
    }

    @Override
    public StudentResponseDTO findByID(long id) {
        Optional<Student> studentOptional = studentRepository.findById(id);
        if (studentOptional.isPresent()) {
            return studentResponseMapper.entityToDto(studentOptional.get());
        }
        return null;
    }

    @Override
    public StudentResponseDTO findByLogin(String login) {
        Optional<Student> studentOptional = studentRepository.findByLogin(login);
        if (studentOptional.isPresent()) {
            return studentResponseMapper.entityToDto(studentOptional.get());
        }
        return null;
    }

    @Override
    public List<StudentResponseDTO> findByUniversityName(String universityName) {
        Optional<University> universityOptional = universityRepository.findByUniversityName(universityName);
        if (universityOptional.isPresent()) {
            return studentResponseMapper.entitiesToDtos(userStudentResponseMapper.usersToStudents(universityOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<StudentResponseDTO> findByFacultyName(String facultyName) {
        Optional<Faculty> facultyOptional = facultyRepository.findByFacultyName(facultyName);
        if (facultyOptional.isPresent()) {
            return studentResponseMapper.entitiesToDtos(userStudentResponseMapper.usersToStudents(facultyOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<StudentResponseDTO> findByDegreeName(String degreeName) {
        Optional<Degree> degreeOptional = degreeRepository.findByDegreeName(degreeName);
        if (degreeOptional.isPresent()) {
            return studentResponseMapper.entitiesToDtos(userStudentResponseMapper.usersToStudents(degreeOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<StudentResponseDTO> findByRoleName(String roleName) {
        Optional<Role> roleOptional = roleRepository.findByRoleName(roleName);
        if (roleOptional.isPresent()) {
            return studentResponseMapper.entitiesToDtos(userStudentResponseMapper.usersToStudents(roleOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<StudentResponseDTO> findAll() {
        return studentResponseMapper.entitiesToDtos(studentRepository.findAll());
    }

    @Override
    public void deleteById(long id) {
        studentRepository.deleteById(id);
    }
}
