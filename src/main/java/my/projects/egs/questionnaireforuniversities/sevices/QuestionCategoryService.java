package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCategoryDTO;

import java.util.List;
import java.util.Optional;

public interface QuestionCategoryService {

    QuestionCategoryDTO save(QuestionCategoryDTO questionCategoryDTO);

    QuestionCategoryDTO findByID(long id);

    QuestionCategoryDTO findByCategoryName(String categoryName);

    List<QuestionCategoryDTO> findAll();

    void deleteById(int id);

}
