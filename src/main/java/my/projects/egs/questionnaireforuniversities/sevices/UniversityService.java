package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;

import java.util.List;
import java.util.Optional;

public interface UniversityService {

    UniversityDTO save(UniversityDTO questionDTO);

    UniversityDTO findByID(long id);

    UniversityDTO findByUniversityName(String universityName);

    List<UniversityDTO> findAll();

    void deleteById(int id);

}
