package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerDTO;

import java.util.List;

public interface AnswerService {

    AnswerDTO save(AnswerDTO answerDTO);

    AnswerDTO findByID(long id);

    List<AnswerDTO> findAll();

    List<AnswerDTO> findByUserId(long id);

    List<AnswerDTO> findByQuestionID(long id);

    void deleteById(long id);

}
