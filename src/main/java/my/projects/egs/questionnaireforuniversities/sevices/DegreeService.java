package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.DegreeDTO;

import java.util.List;
import java.util.Optional;

public interface DegreeService {

    DegreeDTO save(DegreeDTO degreeDTO);

    DegreeDTO findByID(long id);

    DegreeDTO findByDegreeName(String degreeName);

    List<DegreeDTO> findAll();

    void deleteById(short id);

}
