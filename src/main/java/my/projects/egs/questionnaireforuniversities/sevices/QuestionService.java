package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionDTO;

import java.util.List;

public interface QuestionService {

    QuestionDTO save(QuestionDTO questionDTO);

    QuestionDTO findByID(long id);

    List<QuestionDTO> findByCategoryName(String categoryName);

    List<QuestionDTO> findByUserId(long id);

    List<QuestionDTO> findAll();

    void deleteById(long id);

}
