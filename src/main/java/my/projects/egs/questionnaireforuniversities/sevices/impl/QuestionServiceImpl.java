package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionDTO;
import my.projects.egs.questionnaireforuniversities.entities.Question;
import my.projects.egs.questionnaireforuniversities.entities.QuestionCategory;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.exceptions.questionreleated.QuestionNotFoundException;
import my.projects.egs.questionnaireforuniversities.mappers.QuestionMapper;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionCategoryRepository;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {

    private QuestionMapper questionMapper;
    private QuestionRepository questionRepository;
    private QuestionCategoryRepository questionCategoryRepository;
    private UserRepository userRepository;

    public QuestionServiceImpl(QuestionMapper questionMapper, QuestionRepository questionRepository,
                               QuestionCategoryRepository questionCategoryRepository, UserRepository userRepository) {
        this.questionMapper = questionMapper;
        this.questionRepository = questionRepository;
        this.questionCategoryRepository = questionCategoryRepository;
        this.userRepository = userRepository;
    }

    @Override
    public QuestionDTO save(QuestionDTO questionDTO) {
//        Long userId = 15l; // TODO Get from SecurityContextHolder
//
//        Question question = questionMapper.dtoToEntity(questionDTO);
//        question.setUser(userRepository.getOne(userId));
//
//        question = questionRepository.save(question);
//
//        return questionMapper.entityToDto(question);

        if (questionDTO.getCreatedAt() == null) {
            questionDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        questionDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return questionMapper.entityToDto(questionRepository.save(questionMapper.dtoToEntity(questionDTO)));
    }

    @Override
    public QuestionDTO findByID(long id) {
        Optional<Question> questionOptional = questionRepository.findById(id);
        if (questionOptional.isPresent()) {
            return questionMapper.entityToDto(questionOptional.get());
        }else throw new QuestionNotFoundException(id);
    }

    @Override
    public List<QuestionDTO> findByCategoryName(String categoryName) {
        Optional<QuestionCategory> questionCategoryOptional = questionCategoryRepository.findByCategoryName(categoryName);
        if (questionCategoryOptional.isPresent()) {
            return questionMapper.entitiesToDtos(questionCategoryOptional.get().getQuestions());
        }
        return Collections.emptyList();
    }

    @Override
    public List<QuestionDTO> findByUserId(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return questionMapper.entitiesToDtos(userOptional.get().getQuestions());
        }
        return Collections.emptyList();
    }

    @Override
    public List<QuestionDTO> findAll() {
        return questionMapper.entitiesToDtos(questionRepository.findAll());
    }

    @Override
    public void deleteById(long id) {
        questionRepository.deleteById(id);
    }
}
