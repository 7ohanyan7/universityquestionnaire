package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.UniversityDTO;
import my.projects.egs.questionnaireforuniversities.entities.University;
import my.projects.egs.questionnaireforuniversities.mappers.UniversityMapper;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.sevices.UniversityService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UniversityServiceImpl implements UniversityService {

    private UniversityMapper universityMapper;
    private UniversityRepository universityRepository;

    public UniversityServiceImpl(UniversityMapper universityMapper, UniversityRepository universityRepository) {
        this.universityMapper = universityMapper;
        this.universityRepository = universityRepository;
    }

    @Override
    public UniversityDTO save(UniversityDTO universityDTO) {
        if (universityDTO.getCreatedAt() == null) {
            universityDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        universityDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return universityMapper.entityToDto(universityRepository.save(universityMapper.dtoToEntity(universityDTO)));
    }

    @Override
    public UniversityDTO findByID(long id) {
        Optional<University> universityOptional = universityRepository.findById((int) id);
        if (universityOptional.isPresent()) {
            return universityMapper.entityToDto(universityOptional.get());
        }
        return null;
    }

    @Override
    public UniversityDTO findByUniversityName(String universityName) {
        Optional<University> universityOptional = universityRepository.findByUniversityName(universityName);
        if (universityOptional.isPresent()) {
            return universityMapper.entityToDto(universityOptional.get());
        }
        return null;
    }

    @Override
    public List<UniversityDTO> findAll() {
        return universityMapper.entitiesToDtos(universityRepository.findAll());
    }

    @Override
    public void deleteById(int id) {
        universityRepository.deleteById(id);
    }
}
