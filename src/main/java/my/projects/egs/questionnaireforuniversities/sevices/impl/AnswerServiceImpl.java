package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerDTO;
import my.projects.egs.questionnaireforuniversities.entities.Answer;
import my.projects.egs.questionnaireforuniversities.entities.Question;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.mappers.AnswerMapper;
import my.projects.egs.questionnaireforuniversities.repositories.AnswerRepository;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.sevices.AnswerService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswerService {

    private AnswerMapper answerMapper;
    private AnswerRepository answerRepository;
    private UserRepository userRepository;
    private QuestionRepository questionRepository;

    public AnswerServiceImpl(AnswerMapper answerMapper, AnswerRepository answerRepository, UserRepository userRepository,
                             QuestionRepository questionRepository) {
        this.answerMapper = answerMapper;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
        this.questionRepository = questionRepository;
    }

    @Override
    public AnswerDTO save(AnswerDTO answerDTO) {
        if (answerDTO.getCreatedAt() == null) {
            answerDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        answerDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return answerMapper.entityToDto(answerRepository.save(answerMapper.dtoToEntity(answerDTO)));
    }

    @Override
    public AnswerDTO findByID(long id) {
        Optional<Answer> answerOptional = answerRepository.findById(id);
        if (answerOptional.isPresent()) {
            return answerMapper.entityToDto(answerOptional.get());
        }
        return null;
    }

    @Override
    public List<AnswerDTO> findAll() {
        return answerMapper.entitiesToDtos(answerRepository.findAll());
    }

    @Override
    public List<AnswerDTO> findByUserId(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            return answerMapper.entitiesToDtos(userOptional.get().getAnswers());
        }
        return Collections.emptyList();
    }

    @Override
    public List<AnswerDTO> findByQuestionID(long id) {
        Optional<Question> questionOptional = questionRepository.findById(id);
        if (questionOptional.isPresent()) {
            return answerMapper.entitiesToDtos(questionOptional.get().getAnswers());
        }
        return Collections.emptyList();
    }

    @Override
    public void deleteById(long id) {
        answerRepository.deleteById(id);
    }
}
