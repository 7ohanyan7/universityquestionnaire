package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCommentDTO;

import java.util.List;
import java.util.Optional;

public interface QuestionCommentService {

    QuestionCommentDTO save(QuestionCommentDTO questionCommentDTO);

    QuestionCommentDTO findByID(long id);

    List<QuestionCommentDTO> findByUserLogin(String userLogin);

    List<QuestionCommentDTO> findByQuestionId(long id);

    List<QuestionCommentDTO> findAll();

    void deleteById(long id);

}
