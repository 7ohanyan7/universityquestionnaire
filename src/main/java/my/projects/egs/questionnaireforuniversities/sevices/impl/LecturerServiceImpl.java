package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.LecturerRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.LecturerResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.*;
import my.projects.egs.questionnaireforuniversities.mappers.user.LecturerRequestMapper;
import my.projects.egs.questionnaireforuniversities.mappers.user.LecturerResponseMapper;
import my.projects.egs.questionnaireforuniversities.mappers.user.userlecturer.UserLecturerResponseMapper;
import my.projects.egs.questionnaireforuniversities.repositories.*;
import my.projects.egs.questionnaireforuniversities.sevices.LecturerService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class LecturerServiceImpl implements LecturerService {

    private UserLecturerResponseMapper userLecturerResponseMapper;
    private LecturerResponseMapper lecturerResponseMapper;
    private LecturerRequestMapper lecturerRequestMapper;
    private LecturerRepository lecturerRepository;
    private UniversityRepository universityRepository;
    private FacultyRepository facultyRepository;
    private DegreeRepository degreeRepository;
    private RoleRepository roleRepository;

    public LecturerServiceImpl(UserLecturerResponseMapper userLecturerResponseMapper, LecturerResponseMapper lecturerResponseMapper,
                               LecturerRequestMapper lecturerRequestMapper, LecturerRepository lecturerRepository,
                               UniversityRepository universityRepository, FacultyRepository facultyRepository,
                               DegreeRepository degreeRepository,
                               RoleRepository roleRepository) {
        this.userLecturerResponseMapper = userLecturerResponseMapper;
        this.lecturerResponseMapper = lecturerResponseMapper;
        this.lecturerRequestMapper = lecturerRequestMapper;
        this.lecturerRepository = lecturerRepository;
        this.universityRepository = universityRepository;
        this.facultyRepository = facultyRepository;
        this.degreeRepository = degreeRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public LecturerResponseDTO save(LecturerRequestDTO lecturerRequestDTO) {
        if (lecturerRequestDTO.getCreatedAt() == null) {
            lecturerRequestDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        lecturerRequestDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return lecturerResponseMapper.entityToDto(lecturerRepository.save(lecturerRequestMapper.dtoToEntity(lecturerRequestDTO)));
    }

    @Override
    public LecturerResponseDTO findByID(long id) {
        Optional<Lecturer> lecturerOptional = lecturerRepository.findById(id);
        if (lecturerOptional.isPresent()) {
            return lecturerResponseMapper.entityToDto(lecturerOptional.get());
        }
        return null;
    }

    @Override
    public LecturerResponseDTO findByLogin(String login) {
        Optional<Lecturer> lecturerOptional = lecturerRepository.findByLogin(login);
        if (lecturerOptional.isPresent()) {
            return lecturerResponseMapper.entityToDto(lecturerOptional.get());
        }
        return null;
    }

    @Override
    public List<LecturerResponseDTO> findByUniversityName(String universityName) {
        Optional<University> universityOptional = universityRepository.findByUniversityName(universityName);
        if (universityOptional.isPresent()) {
            return lecturerResponseMapper.entitiesToDtos(userLecturerResponseMapper.usersToLecturers(universityOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<LecturerResponseDTO> findByFacultyName(String facultyName) {
        Optional<Faculty> facultyOptional = facultyRepository.findByFacultyName(facultyName);
        if (facultyOptional.isPresent()) {
            return lecturerResponseMapper.entitiesToDtos(userLecturerResponseMapper.usersToLecturers(facultyOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<LecturerResponseDTO> findByDegreeName(String degreeName) {
        Optional<Degree> degreeOptional = degreeRepository.findByDegreeName(degreeName);
        if (degreeOptional.isPresent()) {
            return lecturerResponseMapper.entitiesToDtos(userLecturerResponseMapper.usersToLecturers(degreeOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<LecturerResponseDTO> findByRoleName(String roleName) {
        Optional<Role> roleOptional = roleRepository.findByRoleName(roleName);
        if (roleOptional.isPresent()) {
            return lecturerResponseMapper.entitiesToDtos(userLecturerResponseMapper.usersToLecturers(roleOptional.get().getUsers()));
        }
        return Collections.emptyList();
    }

    @Override
    public List<LecturerResponseDTO> findAll() {
        return lecturerResponseMapper.entitiesToDtos(lecturerRepository.findAll());
    }

    @Override
    public void deleteById(long id) {
        lecturerRepository.deleteById(id);
    }
}
