package my.projects.egs.questionnaireforuniversities.sevices;

import java.util.List;

public interface UserService<request, response> {

    response save(request type);

    response findByID(long id);

    response findByLogin(String login);

    List<response> findByUniversityName(String universityName);

    List<response> findByFacultyName(String facultyName);

    List<response> findByDegreeName(String degreeName);

    List<response> findByRoleName(String roleName);

    List<response> findAll();

    void deleteById(long id);

}
