package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.FacultyDTO;
import my.projects.egs.questionnaireforuniversities.entities.Faculty;
import my.projects.egs.questionnaireforuniversities.entities.University;
import my.projects.egs.questionnaireforuniversities.mappers.FacultyMapper;
import my.projects.egs.questionnaireforuniversities.repositories.FacultyRepository;
import my.projects.egs.questionnaireforuniversities.repositories.UniversityRepository;
import my.projects.egs.questionnaireforuniversities.sevices.FacultyService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class FacultyServiceImpl implements FacultyService {

    private FacultyMapper facultyMapper;
    private FacultyRepository facultyRepository;
    private UniversityRepository universityRepository;

    public FacultyServiceImpl(FacultyMapper facultyMapper, FacultyRepository facultyRepository, UniversityRepository universityRepository) {
        this.facultyMapper = facultyMapper;
        this.facultyRepository = facultyRepository;
        this.universityRepository = universityRepository;
    }

    @Override
    public FacultyDTO save(FacultyDTO facultyDTO) {
        if (facultyDTO.getCreatedAt() == null) {
            facultyDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        facultyDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return facultyMapper.entityToDto(facultyRepository.save(facultyMapper.dtoToEntity(facultyDTO)));
    }

    @Override
    public FacultyDTO findByID(long id) {
        Optional<Faculty> facultyOptional = facultyRepository.findById((int) id);
        if (facultyOptional.isPresent()) {
            return facultyMapper.entityToDto(facultyOptional.get());
        }
        return null;
    }

    @Override
    public FacultyDTO findByFacultyName(String facultyName) {
        Optional<Faculty> facultyOptional = facultyRepository.findByFacultyName(facultyName);
        if (facultyOptional.isPresent()) {
            return facultyMapper.entityToDto(facultyOptional.get());
        }
        return null;
    }

    @Override
    public List<FacultyDTO> findByUniversityName(String universityName) {
        Optional<University> universityOptional = universityRepository.findByUniversityName(universityName);
        if (universityOptional.isPresent()) {
            return facultyMapper.entitiesToDtos(universityOptional.get().getFaculties());
        }
        return Collections.emptyList();
    }

    @Override
    public List<FacultyDTO> findAll() {
        return facultyMapper.entitiesToDtos(facultyRepository.findAll());
    }

    @Override
    public void deleteById(int id) {
        facultyRepository.deleteById(id);
    }
}
