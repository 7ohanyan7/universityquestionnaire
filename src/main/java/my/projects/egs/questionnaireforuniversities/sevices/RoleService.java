package my.projects.egs.questionnaireforuniversities.sevices;

import my.projects.egs.questionnaireforuniversities.dtos.RoleDTO;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    
    RoleDTO save(RoleDTO roleDTO);
    
    List<RoleDTO> findAll();

    void deleteById(byte id);

}
