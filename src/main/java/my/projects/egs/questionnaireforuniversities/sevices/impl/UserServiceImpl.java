package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.user.request.UserRequestDTO;
import my.projects.egs.questionnaireforuniversities.dtos.user.response.UserResponseDTO;
import my.projects.egs.questionnaireforuniversities.entities.User;
import my.projects.egs.questionnaireforuniversities.mappers.user.UserResponseMapper;
import my.projects.egs.questionnaireforuniversities.repositories.UserRepository;
import my.projects.egs.questionnaireforuniversities.sevices.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService<UserRequestDTO, UserResponseDTO> {

    private UserRepository userRepository;
    private UserResponseMapper userResponseMapper;

    public UserServiceImpl(UserRepository userRepository, UserResponseMapper userResponseMapper) {
        this.userRepository = userRepository;
        this.userResponseMapper = userResponseMapper;
    }

    @Override
    public UserResponseDTO save(UserRequestDTO type) {
        return null;
    }

    @Override
    public UserResponseDTO findByID(long id) {
        Optional<User> studentOptional = userRepository.findById(id);
        if (studentOptional.isPresent()) {
            return userResponseMapper.entityToDto(studentOptional.get());
        }
        return null;
    }

    @Override
    public UserResponseDTO findByLogin(String login) {
        Optional<User> studentOptional = userRepository.findByLogin(login);
        if (studentOptional.isPresent()) {
            return userResponseMapper.entityToDto(studentOptional.get());
        }
        return null;
    }

    @Override
    public List<UserResponseDTO> findByUniversityName(String universityName) {
        return null;
    }

    @Override
    public List<UserResponseDTO> findByFacultyName(String facultyName) {
        return null;
    }

    @Override
    public List<UserResponseDTO> findByDegreeName(String degreeName) {
        return null;
    }

    @Override
    public List<UserResponseDTO> findByRoleName(String roleName) {
        return null;
    }

    @Override
    public List<UserResponseDTO> findAll() {
        return null;
    }

    @Override
    public void deleteById(long id) {

    }
}
