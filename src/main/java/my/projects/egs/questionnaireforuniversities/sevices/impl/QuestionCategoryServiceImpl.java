package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.QuestionCategoryDTO;
import my.projects.egs.questionnaireforuniversities.entities.QuestionCategory;
import my.projects.egs.questionnaireforuniversities.mappers.QuestionCategoryMapper;
import my.projects.egs.questionnaireforuniversities.repositories.QuestionCategoryRepository;
import my.projects.egs.questionnaireforuniversities.sevices.QuestionCategoryService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionCategoryServiceImpl implements QuestionCategoryService {

    private QuestionCategoryMapper questionCategoryMapper;
    private QuestionCategoryRepository questionCategoryRepository;

    public QuestionCategoryServiceImpl(QuestionCategoryMapper questionCategoryMapper, QuestionCategoryRepository questionCategoryRepository) {
        this.questionCategoryMapper = questionCategoryMapper;
        this.questionCategoryRepository = questionCategoryRepository;
    }

    @Override
    public QuestionCategoryDTO save(QuestionCategoryDTO questionCategoryDTO) {
        if (questionCategoryDTO.getCreatedAt() == null) {
            questionCategoryDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        questionCategoryDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return questionCategoryMapper
                .entityToDto(questionCategoryRepository
                        .save(questionCategoryMapper
                                .dtoToEntity(questionCategoryDTO)));
    }

    @Override
    public QuestionCategoryDTO findByID(long id) {
        Optional<QuestionCategory> questionCategoryOptional = questionCategoryRepository.findById((int) id);
        if (questionCategoryOptional.isPresent()) {
            return questionCategoryMapper.entityToDto(questionCategoryOptional.get());
        }
        return null;
    }

    @Override
    public QuestionCategoryDTO findByCategoryName(String categoryName) {
        Optional<QuestionCategory> questionCategoryOptional = questionCategoryRepository.findByCategoryName(categoryName);
        if (questionCategoryOptional.isPresent()) {
            return questionCategoryMapper.entityToDto(questionCategoryOptional.get());
        }
        return null;
    }

    @Override
    public List<QuestionCategoryDTO> findAll() {
        return questionCategoryMapper.entitiesToDtos(questionCategoryRepository.findAll());
    }

    @Override
    public void deleteById(int id) {
        questionCategoryRepository.deleteById(id);
    }
}
