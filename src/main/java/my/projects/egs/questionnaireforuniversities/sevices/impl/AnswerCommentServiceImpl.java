package my.projects.egs.questionnaireforuniversities.sevices.impl;

import my.projects.egs.questionnaireforuniversities.dtos.AnswerCommentDTO;
import my.projects.egs.questionnaireforuniversities.entities.Answer;
import my.projects.egs.questionnaireforuniversities.entities.AnswerComment;
import my.projects.egs.questionnaireforuniversities.mappers.AnswerCommentMapper;
import my.projects.egs.questionnaireforuniversities.repositories.AnswerCommentRepository;
import my.projects.egs.questionnaireforuniversities.repositories.AnswerRepository;
import my.projects.egs.questionnaireforuniversities.sevices.AnswerCommentService;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerCommentServiceImpl implements AnswerCommentService {

    private AnswerCommentMapper answerCommentMapper;
    private AnswerCommentRepository answerCommentRepository;
    private AnswerRepository answerRepository;

    public AnswerCommentServiceImpl(AnswerCommentMapper answerCommentMapper, AnswerCommentRepository answerCommentRepository,
                                    AnswerRepository answerRepository) {
        this.answerCommentMapper = answerCommentMapper;
        this.answerCommentRepository = answerCommentRepository;
        this.answerRepository = answerRepository;
    }

    @Override
    public AnswerCommentDTO save(AnswerCommentDTO answerCommentDTO) {
        if (answerCommentDTO.getCreatedAt() == null) {
            answerCommentDTO.setCreatedAt(Date.valueOf(LocalDate.now()));
        }
        answerCommentDTO.setUpdatedAt(Date.valueOf(LocalDate.now()));
        return answerCommentMapper.entityToDto(answerCommentRepository.save(answerCommentMapper.dtoToEntity(answerCommentDTO)));
    }

    @Override
    public AnswerCommentDTO findByID(long id) {
        Optional<AnswerComment> answerCommentOptional = answerCommentRepository.findById(id);
        if (answerCommentOptional.isPresent()) {
            return answerCommentMapper.entityToDto(answerCommentOptional.get());
        }
        return null;
    }

    @Override
    public List<AnswerCommentDTO> findAll() {
        return answerCommentMapper.entitiesToDtos(answerCommentRepository.findAll());
    }

    @Override
    public List<AnswerCommentDTO> findByAnswerId(long id) {
        Optional<Answer> answerOptional = answerRepository.findById(id);
        if (answerOptional.isPresent()) {
            return answerCommentMapper.entitiesToDtos(answerOptional.get().getComments());
        }
        return Collections.emptyList();
    }

    @Override
    public void deleteById(long id) {
        answerCommentRepository.deleteById(id);
    }
}

